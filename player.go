// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// INTERFACES
//////////////////////

type Player interface {
	GetName() string
	SetName(string)
	GetColor() sf.Color
	AIControlled() bool
	Update(time.Duration)
}

//////////////////////
// STRUCTS
//////////////////////

type HumanPlayer struct {
	planetSelector   *PlanetSelector
	game             *Game
	color            sf.Color
	fleetAttackRatio float32
	fleetRatioIndex  int
	name             string
}

//////////////////////
// FUNCS
//////////////////////

func NewHumanPlayer(game *Game, color sf.Color) *HumanPlayer {
	player := new(HumanPlayer)

	player.game = game
	player.fleetAttackRatio = 0.6

	//color
	player.color = color

	//planet selector
	player.planetSelector = NewPlanetSelector(game, player)

	return player
}

func (this *HumanPlayer) SetName(name string) {
	this.name = name
}

func (this *HumanPlayer) GetName() string {
	return this.name
}

func (this *HumanPlayer) GetColor() sf.Color {
	return this.color
}

func (this *HumanPlayer) AIControlled() bool {
	return false
}

func (this *HumanPlayer) Update(time.Duration) {
	mPos := this.game.imgr.mousePosition
	cPos := this.game.smgr.renderWindow.MapPixelToCoords(mPos, nil)

	planet := this.game.world.FindPlanetFromPosition(Vector2f(cPos))

	if this.game.imgr.IsMouseButtonDown(sf.MouseLeft) {
		this.planetSelector.SetPlanets(planet)
	} else {
		//send ships
		sourcePlanets, destinationPlanet, valid := this.planetSelector.GetPlanets()
		if valid {
			for _, p := range sourcePlanets {
				p.SendShips(this.fleetAttackRatio, destinationPlanet)
			}
		}
		this.planetSelector.Reset()
	}

	if this.game.imgr.doubleClicked && planet != nil {
		//send ships from all planets
		for _, p := range this.game.world.Planets {
			if p.owner == this {
				p.SendShips(this.fleetAttackRatio, planet)
			}
		}
	}
}

//satisfly Player interface
var _ Player = (*HumanPlayer)(nil)
