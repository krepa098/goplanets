// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
)

//////////////////////
// FUNCS
//////////////////////

func RadToDeg(rad float32) float32 {
	return rad * 180.0 / math32.Pi
}

func LoadTexture(path string) *sf.Texture {
	tex, err := sf.NewTextureFromFile(path, nil)

	if err != nil {
		panic("Texture not found")
	}
	tex.SetSmooth(true)

	return tex
}

func Slerp(a, b, t float32) float32 {
	compA := complex(math32.Cos(a), math32.Sin(a))
	compB := complex(math32.Cos(b), math32.Sin(b))

	dot := real(compA)*real(compB) + imag(compA)*imag(compB)

	if dot > 0.9995 {
		return a
	}

	o := math32.Acos(dot)

	comp := complex((math32.Sin((1.0-t)*o)/math32.Sin(o))*real(compA)+(math32.Sin(t*o)/math32.Sin(o))*real(compB),
		(math32.Sin((1.0-t)*o)/math32.Sin(o))*imag(compA)+(math32.Sin(t*o)/math32.Sin(o))*imag(compB))

	return math32.Atan2(imag(comp), real(comp))
}

func Lerp(a, b, t float32) float32 {
	return a*(1.0-t) + b*t
}
