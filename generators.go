// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"math/rand"
	"time"
)

const (
	MINPLANET_SIZE        = 26
	MAXPLANET_SIZE        = 58
	PLANET_SPACING        = 48
	DEFAULT_PLANET_REFILL = 1.5 * 1e4 * time.Millisecond
)

//////////////////////
// FUNCS
//////////////////////

//create random galaxy
func GenerateRandomGalaxy(game *Game, playerCount int, AIOnly bool, planetCount int) (size, center Vector2f) {
	var (
		placementMap  PlacementMap
		currentPlanet = 0
		currentTry    = 0
	)

	//set galaxy bounds
	placementMap.SetBounds(sf.FloatRect{Left: MAXPLANET_SIZE, Top: MAXPLANET_SIZE, Width: float32(game.width) - 2*MAXPLANET_SIZE, Height: float32(game.height) - 2*MAXPLANET_SIZE})

	for currentPlanet < planetCount && currentTry < 1e2 {
		currentTry++

		r := MINPLANET_SIZE + rand.Float32()*(MAXPLANET_SIZE-MINPLANET_SIZE)
		pos, ok := placementMap.GetRandomPlacement(r)

		if !ok {
			continue //spot already occupied
		}

		//neutral player
		owner := Player(nil)

		// players start with the biggest planet
		if currentPlanet < playerCount {
			r = MAXPLANET_SIZE
			owner = game.world.Players[currentPlanet]
		}

		//create new planet
		planet := NewPlanet(game, pos, r, owner)
		currentPlanet++

		//let the placement map know that this spot is now occupied
		placementMap.AddEntry(pos, r+PLANET_SPACING)

		//Planets owned by players have the same amount of ships
		if owner != nil {
			planet.ships = 30
		} else {
			planet.ships /= 2
		}

		game.world.AddPlanet(planet)
	}

	//Adjust global vars
	SHIP_SPEED = 85

	//return galaxy size
	return Vector2f{float32(game.width), float32(game.height)}, Vector2f{float32(game.width), float32(game.height)}.Scale(0.5)
}
