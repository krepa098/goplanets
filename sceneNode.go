// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// INTERFACES
//////////////////////

type SceneNode interface {
	GetLayer() int
	Draw(target sf.RenderTarget, dt time.Duration)
	IsVisible() bool
}

type Selector interface {
	IsInsode(pos Vector2f) bool
}

//////////////////////
// STRUCTS
//////////////////////

type SceneNodes []SceneNode

//////////////////////
// FUNCS
//////////////////////

//sort
func (s SceneNodes) Len() int {
	return len(s)
}

//sort
func (s SceneNodes) Less(i, j int) bool {
	return s[i].GetLayer() < s[j].GetLayer()
}

//sort
func (s SceneNodes) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
