// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
)

//////////////////////
// STRUCTS
//////////////////////

type Vector2f sf.Vector2f

//////////////////////
// FUNCS
//////////////////////

func (this Vector2f) GetLength() float32 {
	return math32.Sqrt(this.X*this.X + this.Y*this.Y)
}

func (this Vector2f) GetLengthSQ() float32 {
	return this.X*this.X + this.Y*this.Y
}

func (this Vector2f) Scale(scale float32) Vector2f {
	return Vector2f{this.X * scale, this.Y * scale}
}

func (this Vector2f) Plus(other Vector2f) Vector2f {
	return Vector2f{this.X + other.X, this.Y + other.Y}
}

func (this Vector2f) Minus(other Vector2f) Vector2f {
	return Vector2f{this.X - other.X, this.Y - other.Y}
}
