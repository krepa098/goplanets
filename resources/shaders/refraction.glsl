uniform sampler2D texture0;
uniform sampler2D texture1;

uniform vec2 textureSize;

const float refIntensity = 8.0;
const vec4 splashColor = vec4(0.7,0.85,0.9,1.0);

void main()
{
    vec2 pos = vec2(gl_TexCoord[0].x, gl_TexCoord[0].y);
    vec4 refrColor = texture2D(texture0,pos);
    vec4 realColor = splashColor * refrColor.b;
    
    vec4 c = texture2D(texture1,pos + vec2(refrColor.r,refrColor.g)*refIntensity/textureSize) + realColor;

    gl_FragColor = c;
}