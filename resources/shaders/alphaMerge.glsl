uniform sampler2D texture0;
const float alpha = 0.25;

void main()
{
    vec2 pos = vec2(gl_TexCoord[0].x, gl_TexCoord[0].y);
    vec4 color = texture2D(texture0,pos);
    
    color.a = min(color.a,alpha);

    gl_FragColor = color;
}