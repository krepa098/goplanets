// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// VARS
//////////////////////

var (
	SplashTexture1, _ = sf.NewTextureFromFile("resources/gfx/splash_nm_ships.png", nil)
	SplashTexture2, _ = sf.NewTextureFromFile("resources/gfx/splash_nm_planets.png", nil)
)

//////////////////////
// STRUCTS
//////////////////////

type SplashEffect struct {
	sprite                *sf.Sprite
	accu                  time.Duration
	game                  *Game
	visible               bool
	startingSize, endSize float32
	duration              time.Duration
	size                  sf.Vector2f
	layer                 int
	color                 sf.Color
}

//////////////////////
// FUNCS
//////////////////////

func NewSplashEffect(game *Game) *SplashEffect {
	splash := new(SplashEffect)
	splash.game = game

	splash.sprite, _ = sf.NewSprite(nil)
	game.smgr.AddSceneNode(splash)

	return splash
}

func (this *SplashEffect) Setup(position Vector2f, color sf.Color, texture *sf.Texture, startingSize, endSize float32, duration time.Duration) {
	this.sprite.SetPosition(sf.Vector2f(position))
	this.sprite.SetColor(color)
	this.sprite.SetPosition(sf.Vector2f(position))
	this.sprite.SetScale(sf.Vector2f{1, 1})
	this.sprite.SetTexture(texture, true)
	this.size = sf.Vector2f{this.sprite.GetLocalBounds().Width, this.sprite.GetLocalBounds().Height}
	this.startingSize = startingSize
	this.endSize = endSize
	this.duration = duration

	this.sprite.SetOrigin(sf.Vector2f{this.size.X / 2, this.size.Y / 2})
	this.sprite.SetScale(sf.Vector2f{startingSize / this.size.X, startingSize / this.size.Y})

	this.accu = 0
	this.visible = true
	this.color = color
}

func (this *SplashEffect) GetLayer() int {
	return 4
}

func (this *SplashEffect) Draw(target sf.RenderTarget, dt time.Duration) {
	target.Draw(this.sprite, sf.RenderStates{sf.BlendAdd, sf.TransformIdentity(), nil, nil})

	ratio := calcRatio(this.accu, this.duration)
	this.color.A = 255 - byte(255*ratio)
	this.sprite.SetColor(this.color)

	this.sprite.SetScale(sf.Vector2f{Lerp(this.startingSize, this.endSize, ratio) / this.size.X, Lerp(this.startingSize, this.endSize, ratio) / this.size.Y})

	if this.accu >= this.duration {
		this.game.effectPool.Push(this)
		this.visible = false
	}

	this.accu += dt
}

func (this *SplashEffect) IsVisible() bool {
	return this.visible
}

func calcRatio(x time.Duration, max time.Duration) float32 {
	ratio := float32(x) / float32(max)
	if ratio > 1 {
		return 1
	}
	return ratio
}
