// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

//////////////////////
// STRUCTS
//////////////////////

type Pool struct {
	items []interface{}
	ctor  func() interface{}
	grow  bool
}

//////////////////////
// FUNCS
//////////////////////

func NewPool(constructor func() interface{}, size int, grow bool) *Pool {
	q := &Pool{items: make([]interface{}, size), ctor: constructor, grow: grow}

	//init elements
	for i := 0; i < size; i++ {
		q.items[i] = constructor()
	}

	return q
}

func (this *Pool) Pop() interface{} {

	if len(this.items) > 0 {
		item := this.items[len(this.items)-1]
		this.items = this.items[:len(this.items)-1]
		return item
	}

	if this.grow {
		//create a new entity
		return this.ctor()
	}
	return nil
}

func (this *Pool) Push(entity interface{}) {
	this.items = append(this.items, entity)
}
