// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	gui "bitbucket.org/krepa098/acropolis"
	sf "bitbucket.org/krepa098/gosfml2"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"time"
)

//////////////////////
// CONSTS
//////////////////////

const (
	VERSION = "v2.1"

	//Tickers
	PHYSICS_STEP = time.Second / 60 //Hz
)

const (
	STATE_MENU = iota
	STATE_GAME
	STATE_START_GAME
	STATE_GO_MENU
)

const (
	GUI_TEXT_SIZE    = 32
	GUI_TEXT_SPACING = 50
	CORNER_RADIUS    = 20
)

const (
	SETTINGS_PATH = "cfg.json"
)

const (
	COUNT_DOWN = 3 //seconds
)

const ( //Nebula
	MENU_SEED = 245.05936
	MENU_R    = 0.7
	MENU_G    = 1.00
	MENU_B    = 0.95
)

//////////////////////
// VARS
//////////////////////

var (
	CPU_CORES = runtime.NumCPU()
)

//////////////////////
// VARS
//////////////////////

var (
	Player_Colors = [...]sf.Color{
		sf.Color{255, 0, 0, 255},
		sf.Color{0, 255, 0, 255},
		sf.Color{0, 0, 255, 255},
		sf.Color{255, 255, 0, 255},
		sf.Color{255, 0, 255, 255},
		sf.Color{100, 100, 100, 255},
		sf.Color{0, 255, 255, 255},
		sf.Color{170, 90, 0, 255},
	}

	Player_Names = [...]string{
		"red",
		"green",
		"blue",
		"yellow",
		"pink",
		"grey",
		"cyan",
		"brown",
	}
)

//////////////////////
// STRUCTS
//////////////////////
type WindowCreationSettings struct {
	width, height, depth  int
	fullscreen, vsync, AA bool
}

type Game struct {
	//manager
	smgr     *SceneManager
	pmgr     *PhysicsManager
	imgr     *InputManager
	soundmgr *SoundManager
	world    *World

	//scene
	renderWindow *sf.RenderWindow
	nebula       *Nebula

	//ticker
	lastTick time.Time
	timeAccu time.Duration

	//state
	width, height uint
	currentState  int

	//pools
	shipPool   *Pool
	effectPool *Pool

	//views
	menuView *sf.View
	rtsView  *RtsView

	//gui
	guim          *gui.Manager
	menuScreen    *gui.Screen
	hudScreen     *gui.Screen
	strengthMeter *StrengthMeter

	//countdown & winner panel
	counter    int
	hasStarted bool
	winner     Player

	//Settings
	settings       Settings
	recreateWindow bool
	windowSettings WindowCreationSettings
}

type Settings struct {
	Resolution string
	Fullscreen bool
	VSync      bool
}

//////////////////////
// FUNCS
//////////////////////

func NewGame(width, height uint, fullscreen, vsync bool) *Game {
	rand.Seed(time.Now().UnixNano())

	game := new(Game)
	game.width = width
	game.height = height

	//create GUI elements for the main menu
	game.guim = gui.NewManager()
	game.menuScreen, _ = gui.LoadScreen("resources/gui/menu.json")
	game.hudScreen, _ = gui.LoadScreen("resources/gui/hud.json")
	game.strengthMeter = NewStrengthMeter(game)

	game.guim.SetScreen(game.menuScreen)

	game.SetupGui()

	//sfml render window
	game.CreateRenderWindowFromSettings()

	//scenemanager
	game.smgr = NewSceneManager(game.renderWindow)

	//physicsmanager
	game.pmgr = NewPhysicsManager(game)

	//inputmanager
	game.imgr = NewInputManager()

	//world
	game.world = &World{}

	//soundmanager
	game.soundmgr = NewSoundManager()

	game.soundmgr.LoadMusic("resources/sound/beauty_of_chaos.ogg")
	game.soundmgr.LoadMusic("resources/sound/1minute.ogg")
	game.soundmgr.LoadMusic("resources/sound/Szymon Matuszewski - Hope.ogg")

	game.soundmgr.LoopMusic(0, 0)

	//views
	game.menuView = game.renderWindow.GetView()
	game.rtsView = NewRtsView(game.imgr)

	//nebula
	game.nebula = NewNebula()
	game.nebula.SetParams(game.menuView.GetSize().X, game.menuView.GetSize().Y, MENU_SEED, MENU_R, MENU_G, MENU_B)

	return game
}

func (this *Game) StartNewGame(playerCount int, AIOnly bool, planetCount int, spawnRate int) {
	//switch to game state
	this.currentState = STATE_GAME

	//switch gui screens
	this.guim.SetScreen(this.hudScreen)

	//clear
	this.smgr.ClearScene()
	this.pmgr.ClearEntities()
	this.world.Clear()

	this.smgr.AddSceneNode(game.nebula)

	//pools
	this.shipPool = NewPool(func() interface{} { return NewShip(this) }, 1024, true)
	this.effectPool = NewPool(func() interface{} { return NewSplashEffect(this) }, 256, true)

	//check params
	if planetCount < playerCount || playerCount <= 0 || planetCount <= 0 {
		planetCount = playerCount
	}

	//create players. player 0 is human.
	for i := 0; i < playerCount; i++ {
		if i == 0 && !AIOnly {
			this.world.HumanPlayer = NewHumanPlayer(this, Player_Colors[i])
			this.world.AddPlayer(this.world.HumanPlayer)
		} else {
			this.world.AddPlayer(NewAIPlayer(this, Player_Colors[i]))
		}
		this.world.Players[i].SetName(Player_Names[i])
	}

	//set spawn rate
	switch spawnRate {
	case 0: //normal
		PLANET_REFILL = DEFAULT_PLANET_REFILL
	case 1: //high
		PLANET_REFILL = time.Duration(float32(DEFAULT_PLANET_REFILL) * 0.5)
	case 2: //low
		PLANET_REFILL = time.Duration(float32(DEFAULT_PLANET_REFILL) * 2.0)
	}

	//create the planets
	galaxySize := Vector2f{}
	galaxyCenter := Vector2f{}

	galaxySize, galaxyCenter = GenerateRandomGalaxy(this, playerCount, AIOnly, planetCount)
	//resize view
	this.rtsView.Reset(galaxySize, galaxyCenter)

	//resize world (bigger than the actual map)
	margin := Vector2f{200, 200}
	this.pmgr.SetWorldRect(sf.FloatRect{0 - margin.X, 0 - margin.Y, galaxySize.X + margin.X*2, galaxySize.Y + margin.Y*2})

	//ticker
	this.lastTick = time.Now()

	//change music
	this.soundmgr.LoopMusic(1, this.soundmgr.MusicCount())

	if AIOnly {
		this.renderWindow.SetMouseCursorVisible(false)
		findWidget(this.hudScreen, "frame_countdown").SetVisible(false)
		this.counter = 0
	} else {
		this.counter = COUNT_DOWN
		//show countdown
		findWidget(this.hudScreen, "frame_countdown").SetVisible(true)
		findWidget(this.hudScreen, "label_countdown").(*gui.Label).Text = strconv.Itoa(this.counter)
	}

	this.hasStarted = false

	this.nebula.Generate(this.rtsView.view)
}

func (this *Game) Run() {
	// MAINLOOP
	for this.renderWindow.IsOpen() {
		switch this.currentState {
		case STATE_MENU:
			// MAIN MENU
			this.MenuLoop()
		case STATE_GAME:
			// PLAYING
			this.GameLoop()
		}
	}
}

func (this *Game) GameLoop() {
	//poll events
	for event := this.renderWindow.PollEvent(); event != nil; event = this.renderWindow.PollEvent() {
		switch ev := event.(type) {
		case sf.EventKeyReleased:
			switch ev.Code {
			case sf.KeyEscape:
				this.currentState = STATE_MENU
				this.soundmgr.LoopMusic(0, 0)
				this.nebula.SetParams(game.menuView.GetSize().X, game.menuView.GetSize().Y, MENU_SEED, MENU_R, MENU_G, MENU_B)
				this.renderWindow.SetMouseCursorVisible(true)
				this.guim.SetScreen(this.menuScreen)
			}
		case sf.EventClosed:
			this.renderWindow.Close()
		}
		//gui input
		HandleGuiEvent(event, this.guim)

		this.imgr.HandleEvent(event)
	}

	//timing
	now := time.Now()
	dt := now.Sub(this.lastTick)
	this.lastTick = now

	if this.hasStarted {
		this.timeAccu += dt

		if this.timeAccu > 250*time.Millisecond {
			this.timeAccu = 250 * time.Millisecond
		}

		//simulate
		for ; this.timeAccu >= PHYSICS_STEP; this.timeAccu -= PHYSICS_STEP {
			this.pmgr.EndStep() // wait for the simulation to finish

			//this.BenchmarkOutput()

			for i := 0; i < len(this.world.Players); i++ {
				this.world.Players[i].Update(PHYSICS_STEP)
			}

			this.pmgr.StartStep(PHYSICS_STEP)
			this.pmgr.Step()
		}
	} else {
		//count down!
		this.timeAccu += dt
		if this.timeAccu > 1*time.Second {
			this.timeAccu -= 1 * time.Second
			this.counter--
			findWidget(this.hudScreen, "label_countdown").(*gui.Label).Text = strconv.Itoa(this.counter)
		}
		if this.counter == 0 {
			//start!
			this.hasStarted = true
			//hide countdown
			findWidget(this.hudScreen, "frame_countdown").SetVisible(false)
		}
	}

	//update view
	this.rtsView.Update()
	this.renderWindow.SetView(this.rtsView.view)

	// Clear the window
	this.renderWindow.Clear(sf.ColorBlack())

	//render scene
	this.smgr.DrawAll(this.renderWindow, dt)

	//render gui
	oldview := this.renderWindow.GetView()
	this.renderWindow.SetView(this.renderWindow.GetDefaultView())
	game.guim.Draw(GuiPainter, this.renderWindow)
	this.renderWindow.SetView(oldview)

	//Display things on screen
	this.renderWindow.Display()

	//sound
	this.soundmgr.Update()

	//check winner
	if winner, win := this.world.CheckForWinner(); win {
		this.winner = winner
		this.StartNewGame(len(this.world.Players), true, len(this.world.Planets), 2)
	}

	this.imgr.NewFrame()
}

func HandleGuiEvent(event sf.Event, manager *gui.Manager) {
	var uev gui.Event
	switch ev := event.(type) {
	case sf.EventMouseButtonPressed:
		uev = gui.Event{Type: gui.MousePressed, X: ev.X, Y: ev.Y}
	case sf.EventMouseMoved:
		uev = gui.Event{Type: gui.MouseMoved, X: ev.X, Y: ev.Y}
	case sf.EventMouseButtonReleased:
		uev = gui.Event{Type: gui.MouseReleased, X: ev.X, Y: ev.Y}
	case sf.EventMouseWheelMoved:
		uev = gui.Event{Type: gui.MouseWheelMoved, X: ev.X, Y: ev.Y, Delta: ev.Delta}
	case sf.EventKeyReleased:
		uev = gui.Event{Type: gui.KeyReleased, Button: int(ev.Code), Back: ev.Code == sf.KeyBack, Delete: ev.Code == sf.KeyDelete, Left: ev.Code == sf.KeyLeft, Right: ev.Code == sf.KeyRight}
	case sf.EventKeyPressed:
		uev = gui.Event{Type: gui.KeyPressed, Button: int(ev.Code), Back: ev.Code == sf.KeyBack, Delete: ev.Code == sf.KeyDelete, Left: ev.Code == sf.KeyLeft, Right: ev.Code == sf.KeyRight}
	case sf.EventResized:
		manager.Screen().SetGeometry(gui.Geometry{0, 0, int(ev.Width), int(ev.Height)})
	}

	manager.FeedEvent(uev)
}

func (this *Game) MenuLoop() {
	//window resizing
	if this.recreateWindow {
		game.CreateRenderWindow(this.windowSettings.width, this.windowSettings.height, this.windowSettings.depth, this.windowSettings.fullscreen, this.windowSettings.vsync, false)
		this.recreateWindow = false
	}

	// sound
	this.soundmgr.Update()

	// set view
	this.renderWindow.SetView(this.menuView)

	// Clear the window
	this.renderWindow.Clear(sf.ColorBlack())

	// render background
	this.nebula.Draw(this.renderWindow, 0)

	// render gui
	game.guim.Draw(GuiPainter, this.renderWindow)

	// Display things on screen
	this.renderWindow.Display()

	//poll events
	for event := this.renderWindow.PollEvent(); event != nil; event = this.renderWindow.PollEvent() {
		switch ev := event.(type) {
		case sf.EventKeyReleased:
			switch ev.Code {
			case sf.KeyEscape:
				this.renderWindow.Close()
			}
		case sf.EventClosed:
			this.renderWindow.Close()
		}

		//gui input
		HandleGuiEvent(event, this.guim)
	}
}

func (this *Game) SetupGui() {
	var (
		settingsFrame         = findWidget(this.menuScreen, "frame_settings").(*gui.Frame)
		gameplaySettingsFrame = findWidget(this.menuScreen, "frame_gameplay").(*gui.Frame)
		startButton           = findWidget(this.menuScreen, "button_start").(*gui.Button)
		settingsButton        = findWidget(this.menuScreen, "button_settings").(*gui.Button)
		settingsApplyButton   = findWidget(this.menuScreen, "button_apply").(*gui.Button)
		settingsCancelButton  = findWidget(this.menuScreen, "button_cancel").(*gui.Button)
		playerCountSpinbox    = findWidget(this.menuScreen, "spinbox_playercount").(*gui.Spinbox)
		planetCountCombobox   = findWidget(this.menuScreen, "combobox_planetcount").(*gui.Combobox)
		spawnRateCombobox     = findWidget(this.menuScreen, "combobox_spawnrate").(*gui.Combobox)
		resolutionCombobox    = findWidget(this.menuScreen, "combobox_resolution").(*gui.Combobox)
		fullscreenCheckbox    = findWidget(this.menuScreen, "checkbox_fullscreen").(*gui.Checkbox)
		vsyncCheckbox         = findWidget(this.menuScreen, "checkbox_vsync").(*gui.Checkbox)
		versionLabel          = findWidget(this.menuScreen, "label_version").(*gui.Label)
		benchmarkCheckbox     = findWidget(this.menuScreen, "checkbox_spectator").(*gui.Checkbox)
		strengthFrame         = findWidget(this.hudScreen, "frame_strength").(*gui.Frame)
	)

	versionLabel.Text = VERSION

	//populate resolution combobox
	modes := sf.GetFullscreenModes()
	modeItems := make([]string, 0)
	for i := 0; i < len(modes); i++ {
		if modes[i].BitsPerPixel < 32 {
			continue
		}

		str := strconv.Itoa(int(modes[i].Width)) + "x" + strconv.Itoa(int(modes[i].Height))
		modeItems = append(modeItems, str)

	}
	resolutionCombobox.SetItems(modeItems)

	startButton.OnEvent = func(sender gui.Widgeter, ev gui.Event) bool {
		if ev.Type == gui.ButtonClicked {
			playerCount, _ := strconv.Atoi(playerCountSpinbox.CurrentItem())
			planetCount, _ := strconv.Atoi(planetCountCombobox.CurrentItem())
			spawnRate, _ := strconv.Atoi(spawnRateCombobox.CurrentItem())
			benchmark := benchmarkCheckbox.Checked

			this.StartNewGame(playerCount, benchmark, planetCount, spawnRate)
		}
		return false
	}

	settingsButton.OnEvent = func(sender gui.Widgeter, ev gui.Event) bool {
		if ev.Type == gui.ButtonClicked {
			if settingsFrame != nil && gameplaySettingsFrame != nil {
				settingsFrame.SetVisible(true)
				gameplaySettingsFrame.SetVisible(false)

				//restore control states
				fullscreenCheckbox.Checked = this.settings.Fullscreen
				vsyncCheckbox.Checked = this.settings.VSync

				for i := 0; i < resolutionCombobox.ItemCount(); i++ {
					if text := resolutionCombobox.Item(i); text == game.settings.Resolution {
						resolutionCombobox.SetCurrentItem(i)
						break
					}
				}
			}
		}
		return false
	}

	settingsApplyButton.OnEvent = func(sender gui.Widgeter, ev gui.Event) bool {
		if ev.Type == gui.ButtonClicked {
			if settingsFrame != nil && gameplaySettingsFrame != nil {
				settingsFrame.SetVisible(false)
				gameplaySettingsFrame.SetVisible(true)

				fmt.Sscanf(resolutionCombobox.CurrentItem(), "%dx%d", &this.windowSettings.width, &this.windowSettings.height)
				this.windowSettings.depth = 32

				this.windowSettings.fullscreen = fullscreenCheckbox.Checked
				this.windowSettings.vsync = vsyncCheckbox.Checked

				this.recreateWindow = true

				//save settings
				game.settings.Fullscreen = this.windowSettings.fullscreen
				game.settings.Resolution = strconv.Itoa(int(this.windowSettings.width)) + "x" + strconv.Itoa(this.windowSettings.height)
				game.settings.VSync = this.windowSettings.vsync

				data, _ := json.Marshal(game.settings)
				ioutil.WriteFile(SETTINGS_PATH, data, os.ModePerm)
			}
		}
		return false
	}

	settingsCancelButton.OnEvent = func(sender gui.Widgeter, ev gui.Event) bool {
		if ev.Type == gui.ButtonClicked {
			if settingsFrame != nil && gameplaySettingsFrame != nil {
				settingsFrame.SetVisible(false)
				gameplaySettingsFrame.SetVisible(true)
			}
		}
		return false
	}

	strengthFrame.Painter = this.strengthMeter.Draw
}

func (this *Game) CreateRenderWindow(width, height, depth int, fullscreen, vsync, AA bool) {
	//close old render window
	if this.renderWindow != nil {
		this.renderWindow.Close()
	}

	style := sf.StyleDefault ^ sf.StyleResize
	if fullscreen {
		style |= sf.StyleFullscreen
	}

	//create sfml window
	ctx := sf.DefaultContextSettings()
	if AA {
		ctx.DepthBits = 24 //AA is not applied without this
		ctx.AntialiasingLevel = 2
	}

	this.renderWindow = sf.NewRenderWindow(sf.VideoMode{uint(width), uint(height), uint(depth)}, "GoPlanets "+VERSION, style, ctx)
	this.renderWindow.SetVSyncEnabled(vsync)
	this.menuView = this.renderWindow.GetDefaultView()

	img, _ := sf.NewImageFromFile("resources/gfx/icon.png")

	//set icon
	this.renderWindow.SetIcon(img.GetSize().X, img.GetSize().Y, img.GetPixelData())

	this.OnRenderWindowResized(width, height)
}

func (this *Game) OnRenderWindowResized(width, height int) {
	//resize gui
	if this.guim != nil {
		this.menuScreen.SetGeometry(gui.Geometry{0, 0, width, height})
		this.hudScreen.SetGeometry(gui.Geometry{0, 0, width, height})
	}

	//resize views
	if this.rtsView != nil {
		this.rtsView.Reset(Vector2f{float32(width), float32(height)}, Vector2f{})
	}

	this.width = uint(width)
	this.height = uint(height)

	if this.smgr != nil {
		this.smgr.ResizeRenderTargets(this.renderWindow)
	}
}

func (this *Game) CreateRenderWindowFromSettings() {
	//default settings
	var (
		width  = 800
		height = 600
		depth  = 32
	)

	if data, err := ioutil.ReadFile(SETTINGS_PATH); err == nil {
		if err := json.Unmarshal(data, &this.settings); err == nil {
			fmt.Sscanf(this.settings.Resolution, "%dx%d", &width, &height)
		}
	}

	this.CreateRenderWindow(width, height, depth, this.settings.Fullscreen, this.settings.VSync, false)
}
