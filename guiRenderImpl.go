// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	gui "bitbucket.org/krepa098/acropolis"
	sf "bitbucket.org/krepa098/gosfml2"
)

//////////////////////
// STRUCTS
//////////////////////

type Primitives struct {
	frame         *GuiFrame
	text          *sf.Text
	rect          *sf.RectangleShape
	textFont      Font
	pictogramFont Font
}

type Font struct {
	sfFont      *sf.Font
	isPictogram bool
}

var primitives Primitives

func init() {
	primitives.frame = NewGuiFrame()
	primitives.text, _ = sf.NewText(nil)
	primitives.rect, _ = sf.NewRectangleShape()

	primitives.textFont.sfFont, _ = sf.NewFontFromFile("resources/gui/Lato-Regular.ttf")
	primitives.pictogramFont.sfFont, _ = sf.NewFontFromFile("resources/gui/Entypo.ttf")
	primitives.pictogramFont.isPictogram = true
}

//////////////////////
// VARS
//////////////////////

var (
	clipView    *sf.View = sf.NewView()
	clipOldView *sf.View
	frameColor  = sf.Color{65, 65, 65, 200}
)

//////////////////////
// FUNCS
//////////////////////

func DrawText(font Font, geom gui.Geometry, align int, str string, color sf.Color, target sf.RenderTarget) {
	primitives.text.SetFont(font.sfFont)
	primitives.text.SetColor(color)
	primitives.text.SetString(str)

	topOffset := float32(0)
	padding := float32(3)

	if font.isPictogram {
		topOffset = float32(geom.Height)
		primitives.text.SetCharacterSize(uint(geom.Height) * 2)
	} else {
		primitives.text.SetCharacterSize(uint(geom.Height))
		topOffset = float32(font.sfFont.GetGlyph(0070, uint32(geom.Height), false).Bounds.Height) / 4
	}

	switch align {
	case gui.Center:
		primitives.text.SetPosition(sf.Vector2f{float32(geom.Left) + float32(geom.Width)/2 - primitives.text.GetLocalBounds().Width/2, float32(geom.Top) - topOffset})
	case gui.Left:
		primitives.text.SetPosition(sf.Vector2f{float32(geom.Left) + padding, float32(geom.Top) - topOffset})
	case gui.Right:
		primitives.text.SetPosition(sf.Vector2f{float32(geom.Left+geom.Width) - primitives.text.GetLocalBounds().Width - padding, float32(geom.Top) - topOffset})
	}

	primitives.text.Draw(target, sf.DefaultRenderStates())
}

func DrawRect(geom gui.Geometry, color sf.Color, outlineThickness float32, outlineColor sf.Color, target sf.RenderTarget) {
	primitives.rect.SetPosition(sf.Vector2f{float32(geom.Left), float32(geom.Top)})
	primitives.rect.SetSize(sf.Vector2f{float32(geom.Width), float32(geom.Height)})
	primitives.rect.SetFillColor(color)
	primitives.rect.SetOutlineColor(outlineColor)
	primitives.rect.SetOutlineThickness(outlineThickness)
	primitives.rect.Draw(target, sf.DefaultRenderStates())
}

func DrawFrame(frame *gui.Frame, context gui.RenderContext, target sf.RenderTarget) {
	if frame.Property("frame_hidden").Bool() {
		return
	}

	switch context {
	case gui.DefaultContext:
		pos := sf.Vector2f{float32(frame.Geometry().Left), float32(frame.Geometry().Top)}
		size := sf.Vector2f{float32(frame.Geometry().Width), float32(frame.Geometry().Height)}
		primitives.frame.Generate(size.X, size.Y, 0.05, frameColor)
		primitives.frame.SetPosition(pos)
		primitives.frame.Draw(target)
	case gui.ClipContext:
		clipStack.ClipArea(target, frame.Geometry())
	case gui.UnclipContext:
		clipStack.UnclipArea(target)
	}
}

func DrawScrollArea(scrollArea *gui.ScrollArea, context gui.RenderContext, target sf.RenderTarget) {
	switch context {
	case gui.DefaultContext:
		DrawRect(scrollArea.Geometry(), sf.Color{150, 150, 150, 220}, 1, sf.ColorWhite(), target)

		if scrollArea.VSliderVisible() {
			DrawRect(scrollArea.VSliderGeometry(), sf.ColorWhite(), 0, sf.ColorWhite(), target)
		}

		if scrollArea.HSliderVisible() {
			DrawRect(scrollArea.HSliderGeometry(), sf.ColorWhite(), 0, sf.ColorWhite(), target)
		}
	case gui.ClipContext:
		clipStack.ClipArea(target, scrollArea.Geometry().Grow(&gui.AbsMargin{1, 1, 1, 1}))
	case gui.UnclipContext:
		clipStack.UnclipArea(target)
	}
}

func DrawLabel(label *gui.Label, context gui.RenderContext, target sf.RenderTarget) {
	if label.Property("shadow").Bool() {
		DrawText(primitives.textFont, label.Geometry().Move(1, 1), label.Align, label.Text, sf.ColorBlack(), target)
	}

	DrawText(primitives.textFont, label.Geometry(), label.Align, label.Text, sf.ColorWhite(), target)
}

func DrawLineedit(object gui.Widgeter, context gui.RenderContext, target sf.RenderTarget) {
}

func DrawButton(button *gui.Button, context gui.RenderContext, target sf.RenderTarget) {
	color := sf.Color{30, 30, 30, 200}
	if button.State() == gui.ButtonHovered {
		color = sf.Color{30, 30, 120, 200}
	}

	pos := sf.Vector2f{float32(button.Geometry().Left), float32(button.Geometry().Top)}
	size := sf.Vector2f{float32(button.Geometry().Width), float32(button.Geometry().Height)}
	primitives.frame.SetPosition(pos)
	primitives.frame.Generate(size.X, size.Y, 0.4, color)
	primitives.frame.Draw(target)

	DrawText(primitives.textFont, button.Geometry(), gui.Center, button.Text, sf.ColorWhite(), target) //button.Align
}

func DrawCheckbox(checkbox *gui.Checkbox, context gui.RenderContext, target sf.RenderTarget) {
	if checkbox.Checked {
		DrawText(primitives.pictogramFont, checkbox.CheckGeometry(), gui.Left, string(rune('\u2713')), sf.ColorWhite(), target)
	} else {
		DrawText(primitives.pictogramFont, checkbox.CheckGeometry(), gui.Left, string(rune('\u274C')), sf.ColorWhite(), target)
	}
}

func DrawSpinbox(spinbox *gui.Spinbox, context gui.RenderContext, target sf.RenderTarget) {
	DrawText(primitives.textFont, spinbox.Geometry(), gui.Left, spinbox.CurrentItem(), sf.ColorWhite(), target)

	DrawText(primitives.pictogramFont, spinbox.SpinupGeometry(), gui.Left, string(rune('\u229E')), sf.ColorWhite(), target)
	DrawText(primitives.pictogramFont, spinbox.SpindownGeometry(), gui.Left, string(rune('\u229F')), sf.ColorWhite(), target)
}

func DrawCombobox(combobox *gui.Combobox, context gui.RenderContext, target sf.RenderTarget) {
	if context == gui.DefaultContext {
		DrawRect(combobox.Geometry(), sf.Color{150, 150, 150, 255}, 1, sf.ColorWhite(), target)
		DrawText(primitives.textFont, combobox.Geometry(), gui.Left, combobox.CurrentItem(), sf.ColorWhite(), target)
		DrawText(primitives.pictogramFont, combobox.Geometry(), gui.Right, string(rune('\u25BE')), sf.ColorWhite(), target)
	}
}

func Spinup(geom gui.Geometry) gui.Geometry {
	return gui.Geometry{geom.Left + geom.Width - geom.Height, geom.Top, geom.Height, geom.Height / 2}
}

func Spindown(geom gui.Geometry) gui.Geometry {
	return gui.Geometry{geom.Left + geom.Width - geom.Height, geom.Top + geom.Height/2, geom.Height, geom.Height / 2}
}

func Checkbox(geom gui.Geometry) gui.Geometry {
	return gui.Geometry{geom.Left + geom.Width - geom.Height, geom.Top, geom.Height, geom.Height}
}

func GuiPainter(object gui.Widgeter, context gui.RenderContext, userData interface{}) {
	target := userData.(sf.RenderTarget)
	switch o := object.(type) {
	case *gui.Frame:
		DrawFrame(o, context, target)
	case *gui.Label:
		DrawLabel(o, context, target)
	case *gui.Checkbox:
		DrawCheckbox(o, context, target)
	case *gui.Spinbox:
		DrawSpinbox(o, context, target)
	case *gui.Combobox:
		DrawCombobox(o, context, target)
	case *gui.Button:
		DrawButton(o, context, target)
	case *gui.Lineedit:
		DrawLineedit(o, context, target)
	case *gui.ScrollArea:
		DrawScrollArea(o, context, target)
	}
}
