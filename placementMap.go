// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"math/rand"
)

//////////////////////
// STRUCTS
//////////////////////

type PlacementMapData struct {
	position Vector2f
	radius   float32
}

type PlacementMap struct {
	data   []PlacementMapData
	bounds sf.FloatRect
}

//////////////////////
// FUNCS
//////////////////////

func (this *PlacementMap) AddEntry(pos Vector2f, radius float32) {
	this.data = append(this.data, PlacementMapData{pos, radius})
}

func (this *PlacementMap) SetBounds(bounds sf.FloatRect) {
	this.bounds = bounds
}

func (this *PlacementMap) IsCovered(data PlacementMapData) bool {
	for i := 0; i < len(this.data); i++ {
		l := this.data[i].position.Minus(data.position).GetLength()
		if l <= this.data[i].radius+data.radius {
			return true
		}
	}
	return false
}

func (this *PlacementMap) GetRandomPlacement(radius float32) (pos Vector2f, sucess bool) {
	for i := 0; i < 20; i++ {
		pos.X, pos.Y = this.bounds.Left+rand.Float32()*this.bounds.Width, this.bounds.Top+rand.Float32()*this.bounds.Height
		if !this.IsCovered(PlacementMapData{pos, radius}) {
			return pos, true
		}
	}
	return Vector2f{}, false
}
