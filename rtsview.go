// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
)

//////////////////////
// CONSTS
//////////////////////

const (
	CAM_SPEED  = 0.01
	MAX_ZOOM   = 1.0
	MIN_ZOOM   = 0.3
	ZOOM_DELTA = 1.2
)

//////////////////////
// STRUCTS
//////////////////////

type RtsView struct {
	view  *sf.View
	imgr  *InputManager
	zoom  float32
	speed Vector2f
}

//////////////////////
// FUNCS
//////////////////////

func NewRtsView(imgr *InputManager) *RtsView {
	view := &RtsView{}
	view.view = sf.NewView()
	view.imgr = imgr
	view.zoom = 1

	return view
}

func (this *RtsView) Reset(size, center Vector2f) {
	this.view.SetCenter(sf.Vector2f(center))
	this.view.SetSize(sf.Vector2f(size))
	this.zoom = 1

	this.speed.X = size.X * CAM_SPEED
	this.speed.Y = size.Y * CAM_SPEED
}

func (this *RtsView) Update() {
	if this.imgr.IsKeyDown(sf.KeyLeft) || this.imgr.IsKeyDown(sf.KeyA) {
		this.view.Move(sf.Vector2f{-this.speed.X * this.zoom, 0})
	}

	if this.imgr.IsKeyDown(sf.KeyRight) || this.imgr.IsKeyDown(sf.KeyD) {
		this.view.Move(sf.Vector2f{this.speed.X * this.zoom, 0})
	}

	if this.imgr.IsKeyDown(sf.KeyUp) || this.imgr.IsKeyDown(sf.KeyW) {
		this.view.Move(sf.Vector2f{0, -this.speed.Y * this.zoom})
	}

	if this.imgr.IsKeyDown(sf.KeyDown) || this.imgr.IsKeyDown(sf.KeyS) {
		this.view.Move(sf.Vector2f{0, this.speed.Y * this.zoom})
	}

	if this.imgr.mouseWheelDelta < 0 && this.zoom < MAX_ZOOM {
		this.view.Zoom(ZOOM_DELTA)
		this.zoom += 0.1
	}

	if this.imgr.mouseWheelDelta > 0 && this.zoom > MIN_ZOOM {
		this.view.Zoom(1.0 / ZOOM_DELTA)
		this.zoom -= 0.1
	}
}
