// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
)

//////////////////////
// HELPERS
//////////////////////

func contains(a sf.FloatRect, b sf.FloatRect) bool {
	return a.Left <= b.Left && a.Top <= b.Top && a.Left+a.Width >= b.Left+b.Width && a.Top+a.Height >= b.Top+b.Height
}

func intersects(a sf.FloatRect, b sf.FloatRect) bool {
	return a.Left < b.Left+b.Width && a.Left+a.Width > b.Left && a.Top < b.Top+b.Height && a.Top+a.Height > b.Top
}

//////////////////////
// STRUCTS
//////////////////////

type Quadtree struct {
	leafs    [4]*Quadtree
	hasLeafs bool
	rect     sf.FloatRect
	items    []BoundingBoxer
}

//////////////////////
// INTERFACES
//////////////////////

type BoundingBoxer interface {
	BoundingBox() sf.FloatRect
}

//////////////////////
// FUNCS
//////////////////////

func NewQuadtree(rect sf.FloatRect, depth int) *Quadtree {
	qtree := &Quadtree{rect: rect}
	qtree.items = make([]BoundingBoxer, 64)
	qtree.RecursiveDevide(depth)

	return qtree
}

func (this *Quadtree) Add(item BoundingBoxer) {
	qnode := this.Locate(item.BoundingBox())

	if qnode != nil {
		qnode.items = append(qnode.items, item)
	}
}

func (this *Quadtree) Query(items *[]BoundingBoxer, bbox sf.FloatRect) {
	if intersects(this.rect, bbox) {
		//we contain this item
		*items = append(*items, this.items...)
		if this.hasLeafs {
			//lets see if our leafs also contain it
			for _, quad := range this.leafs {
				quad.Query(items, bbox)
			}
		}
	}
	//we dont contain the bbox so we don't add us to the list
	return
}

func (this *Quadtree) Locate(bbox sf.FloatRect) *Quadtree {
	if contains(this.rect, bbox) {
		//we contain this item
		if this.hasLeafs {
			//lets see if our leafs also contain it -> make it decent the tree as far as possible
			for _, quad := range this.leafs {
				n := quad.Locate(bbox)
				if n != nil {
					return n
				}
			}
		}

		//we contain it and we have no leafs or none of our leafs contain it
		return this
	}
	return nil
}

func (this *Quadtree) Remove(item BoundingBoxer) {
	qnode := this.Locate(item.BoundingBox())

	if qnode != nil {
		for i := 0; i < len(qnode.items); i++ {
			if qnode.items[i] == item {
				//swap
				qnode.items[i], qnode.items[len(qnode.items)-1], qnode.items[len(qnode.items)-1] = qnode.items[len(qnode.items)-1], qnode.items[i], nil
				//pop back
				qnode.items = qnode.items[:len(qnode.items)-1]
			}
		}
	}
}

func (this *Quadtree) Devide() {
	this.hasLeafs = true
	halfWidth := this.rect.Width / 2
	halfHeight := this.rect.Height / 2

	this.leafs[0] = &Quadtree{rect: sf.FloatRect{Left: this.rect.Left, Top: this.rect.Top, Width: halfWidth, Height: halfHeight}}
	this.leafs[1] = &Quadtree{rect: sf.FloatRect{Left: this.rect.Left + halfWidth, Top: this.rect.Top, Width: halfWidth, Height: halfHeight}}
	this.leafs[2] = &Quadtree{rect: sf.FloatRect{Left: this.rect.Left, Top: this.rect.Top + halfHeight, Width: halfWidth, Height: halfHeight}}
	this.leafs[3] = &Quadtree{rect: sf.FloatRect{Left: this.rect.Left + halfWidth, Top: this.rect.Top + halfHeight, Width: halfWidth, Height: halfHeight}}
}

func (this *Quadtree) RecursiveDevide(depth int) {
	if depth == 0 {
		return
	}

	if !this.hasLeafs {
		this.Devide()
	}

	for _, quad := range this.leafs {
		quad.RecursiveDevide(depth - 1)
	}
}

func (this *Quadtree) Count() int {
	counter := len(this.items)
	if this.hasLeafs {
		for _, quad := range this.leafs {
			counter += quad.Count()
		}
	}
	return counter
}

func (this *Quadtree) Clear() {
	this.items = this.items[:0]

	if this.hasLeafs {
		for _, quad := range this.leafs {
			quad.Clear()
		}
	}
}

func (this *Quadtree) dbgDraw(target sf.RenderTarget) {
	renderRect, _ := sf.NewRectangleShape()
	renderRect.SetPosition(sf.Vector2f{this.rect.Left, this.rect.Top})
	renderRect.SetSize(sf.Vector2f{this.rect.Width, this.rect.Height})
	renderRect.SetFillColor(sf.ColorTransparent())
	renderRect.SetOutlineColor(sf.ColorRed())
	renderRect.SetOutlineThickness(1)

	if this.hasLeafs {
		for _, quad := range this.leafs {
			quad.dbgDraw(target)
		}
	}

	target.Draw(renderRect, sf.DefaultRenderStates())
}
