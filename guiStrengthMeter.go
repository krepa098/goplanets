// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	gui "bitbucket.org/krepa098/acropolis"
	sf "bitbucket.org/krepa098/gosfml2"
)

//////////////////////
// STRUCTS
//////////////////////

type StrengthMeter struct {
	data      []StrengthMeterData
	rectShape *sf.RectangleShape
	game      *Game
}

type StrengthMeterData struct {
	color  sf.Color
	offset Vector2f
	size   Vector2f
	value  int
}

//////////////////////
// FUNCS
//////////////////////

func NewStrengthMeter(game *Game) *StrengthMeter {
	strengthMeter := new(StrengthMeter)
	strengthMeter.rectShape, _ = sf.NewRectangleShape()

	return strengthMeter
}

func (this *StrengthMeter) Update(size gui.Size) {
	this.data = this.data[:0]
	total := 0

	//gather data
	for i := 0; i < len(game.world.Players); i++ {
		var data StrengthMeterData
		data.color = game.world.Players[i].GetColor()
		data.color.A = 150
		data.value = game.world.GetShipCountByPLayer(game.world.Players[i])

		this.data = append(this.data, data)

		total += data.value
	}

	//calculate layout
	offset := float32(0)
	for i := 0; i < len(this.data); i++ {
		relWidth := float32(this.data[i].value) / float32(total)
		this.data[i].size.X = relWidth * float32(size.Width)
		this.data[i].size.Y = float32(size.Height)

		this.data[i].offset.X = offset
		this.data[i].offset.Y = 0

		offset += this.data[i].size.X
	}

}

func (this *StrengthMeter) Draw(object gui.Widgeter, context gui.RenderContext, userData interface{}) {
	frame := object.(*gui.Frame)
	target := userData.(sf.RenderTarget)

	//update
	this.Update(frame.ContentGeometry().Size())

	pos := sf.Vector2f{float32(frame.ContentGeometry().Left), float32(frame.ContentGeometry().Top)}
	//Draw
	for i := 0; i < len(this.data); i++ {
		//setup shape
		this.rectShape.SetFillColor(this.data[i].color)
		this.rectShape.SetSize(sf.Vector2f(this.data[i].size))
		this.rectShape.SetPosition(pos.Plus(sf.Vector2f(this.data[i].offset)))

		target.Draw(this.rectShape, sf.DefaultRenderStates())
	}
}
