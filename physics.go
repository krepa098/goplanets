// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
	"sync"
	"time"
)

//////////////////////
// VARS
//////////////////////

var (
	PHYSICS_WORKER = CPU_CORES
)

//////////////////////
// INTERFACES
//////////////////////

type PhysicalEntity interface {
	GetRadius() float32
	GetPosition() Vector2f

	GetVelocity() Vector2f
	SetVelocity(vel Vector2f)

	StartSimulation(time time.Duration)
	EndSimulation()

	IsStatic() bool
	IsActive() bool

	GetGroup() int

	BoundingBoxer
}

//////////////////////
// STRUCTS
//////////////////////

type PhysicsManager struct {
	entities      []PhysicalEntity
	timestep      time.Duration
	workerChannel chan PhysicalEntity
	game          *Game
	quadtree      *Quadtree
	wgroup        sync.WaitGroup
	queries       [][]BoundingBoxer
}

//////////////////////
// FUNCS
//////////////////////

func NewPhysicsManager(game *Game) *PhysicsManager {
	pmgr := &PhysicsManager{game: game, workerChannel: make(chan PhysicalEntity, 32)}

	pmgr.queries = make([][]BoundingBoxer, PHYSICS_WORKER)

	//start the workers
	for i := 0; i < PHYSICS_WORKER; i++ {
		go func(workerId int) {
			pmgr.queries[workerId] = make([]BoundingBoxer, 0, 64)
			for {
				pmgr.HandleCollision(<-pmgr.workerChannel, workerId)
			}
		}(i)
	}

	return pmgr
}

func (this *PhysicsManager) SetWorldRect(rect sf.FloatRect) {
	this.quadtree = NewQuadtree(rect, 4)
}

func (this *PhysicsManager) AddEntity(entity PhysicalEntity) {
	this.entities = append(this.entities, entity)
}

func (this *PhysicsManager) ClearEntities() {
	for i := range this.entities {
		this.entities[i] = nil
	}
	this.entities = this.entities[:0]
}

func (this *PhysicsManager) RemoveEntity(entity PhysicalEntity) {
	for i := 0; i < len(this.entities); i++ {
		if this.entities[i] == entity {
			this.entities[i], this.entities[len(this.entities)-1], this.entities[len(this.entities)-1] = this.entities[len(this.entities)-1], this.entities[i], nil
			this.entities = this.entities[:len(this.entities)-1]
			break
		}
	}
}

func (this *PhysicsManager) StartStep(dt time.Duration) {
	// start simulation
	for _, e := range this.entities {
		e.StartSimulation(dt)
	}

	this.timestep = dt

	c := 0
	for _, e := range this.entities {
		if e.IsActive() {
			this.quadtree.Add(e)
			c++
		}
	}

	this.wgroup.Add(c)
}

func (this *PhysicsManager) HandleCollision(entity PhysicalEntity, qIndex int) {
	if bboxer, ok := entity.(BoundingBoxer); ok {
		this.queries[qIndex] = this.queries[qIndex][:0]
		this.quadtree.Query(&this.queries[qIndex], bboxer.BoundingBox())
		for _, bboxer := range this.queries[qIndex] {
			if other, ok := bboxer.(PhysicalEntity); ok && (other.GetGroup() < 0 || entity.GetGroup() < 0) || other.GetGroup() == entity.GetGroup() {
				if entity != other {
					//circle collision
					obsPos := Vector2f(other.GetPosition())
					entityPos := Vector2f(entity.GetPosition())
					totalR := other.GetRadius() + entity.GetRadius()

					n := entityPos.Minus(obsPos)

					ls := n.X*n.X + n.Y*n.Y

					// collision happened?
					if ls < totalR*totalR {
						l := math32.Sqrt(ls)

						n.X /= l
						n.Y /= l

						// collision
						delta := (totalR - l) / 2

						v := entity.GetVelocity().Minus(n.Scale(delta))

						// prevent entities from getting stuck
						if v.GetLength() < 0.005 {
							v = v.Scale(1 / 0.005)
						}

						entity.SetVelocity(v)
					}
				}
			}
		}
	}
	this.wgroup.Done()
}

func (this *PhysicsManager) Step() {
	// send the entities down the channel
	go func() {
		for _, e := range this.entities {
			if e.IsActive() {
				this.workerChannel <- e
			}
		}
	}()
}

func (this *PhysicsManager) EndStep() {
	// wait for the workers to finish their work
	this.wgroup.Wait()

	// end simulation
	for _, e := range this.entities {
		e.EndSimulation()
	}
	this.quadtree.Clear()
}
