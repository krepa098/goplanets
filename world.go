// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import "github.com/AE9RB/math32"

//////////////////////
// STRUCTS
//////////////////////

type World struct {
	Planets     []*Planet
	Players     []Player
	HumanPlayer *HumanPlayer
	FleetMgr    FleetManager
}

//////////////////////
// FUNCS
//////////////////////

func (this *World) Clear() {
	this.Planets = this.Planets[:0]
	this.Players = this.Players[:0]
	this.HumanPlayer = nil
	this.FleetMgr.Clear()
}

func (this *World) AddPlanet(p *Planet) {
	this.Planets = append(this.Planets, p)
}

func (this *World) AddPlayer(p Player) {
	this.Players = append(this.Players, p)
}

func (this *World) GetShipCountByPLayer(player Player) (c int) {
	for i := 0; i < len(this.Planets); i++ {
		if this.Planets[i].owner == player {
			c += this.Planets[i].ships
		}
	}
	c += this.FleetMgr.GetShipsOwnedByPlayer(player)
	return
}

func (this *World) CheckForWinner() (Player, bool) {
	var winner Player

	for _, e := range this.Players {
		ships := this.GetShipCountByPLayer(e)

		if ships > 0 && winner == nil {
			winner = e
		} else if ships > 0 {
			return nil, false
		}
	}

	return winner, true
}

func (this *World) FindPlanetFromPosition(pos Vector2f) *Planet {
	for i := 0; i < len(this.Planets); i++ {
		if this.Planets[i].IsInside(pos) {
			return this.Planets[i]
		}
	}
	return nil
}

func (this *World) FindClosestPlanetOwnedBy(pivot *Planet, owner Player) *Planet {
	var (
		best *Planet
		dist = float32(math32.MaxFloat32)
	)

	for i := 0; i < len(this.Planets); i++ {
		d := this.Planets[i].GetPosition().Minus(pivot.GetPosition()).GetLengthSQ()
		if d < dist {
			d = dist
			best = this.Planets[i]
		}
	}
	return best
}
