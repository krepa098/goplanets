// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
	"math/rand"
	"time"
)

//////////////////////
// VARS
//////////////////////

var (
	ShipTexture  = LoadTexture("resources/gfx/ship.png")
	GroupCounter = 0

	SHIP_SPEED = float32(160)
)

//////////////////////
// CONSTS
//////////////////////

const (
	SHIP_ROT_SMOOTH  = 0.03
	SHIP_VISUAL_SIZE = 7
	SHIP_PHYS_SIZE   = 9
)

//////////////////////
// STRUCTS
//////////////////////

type Ship struct {
	sprite      *sf.Sprite
	destination *Planet
	game        *Game
	owner       Player
	velocity    Vector2f
	position    Vector2f
	rotation    float32
	radius      float32
	active      bool
	visible     bool
	group       int //fleet id
}

//////////////////////
// FUNCS
//////////////////////

func NewShip(game *Game) *Ship {
	ship := new(Ship)

	ship.sprite, _ = sf.NewSprite(ShipTexture)
	ship.sprite.SetScale(sf.Vector2f{0.4, 0.4})

	ship.game = game
	ship.radius = SHIP_PHYS_SIZE

	//scale + origin
	size := Vector2f{ship.sprite.GetLocalBounds().Width, ship.sprite.GetLocalBounds().Height}
	ship.sprite.SetOrigin(sf.Vector2f{size.X / 2, size.Y / 2})
	ship.sprite.SetScale(sf.Vector2f{SHIP_VISUAL_SIZE * 2 / size.X, SHIP_VISUAL_SIZE * 2 / size.Y})

	//smgr
	game.smgr.AddSceneNode(ship)

	//pmgr
	game.pmgr.AddEntity(ship)

	return ship
}

func (this *Ship) Setup(origin *Planet, destination *Planet, owner Player, group int) {
	//setup
	this.sprite.SetColor(owner.GetColor())
	this.destination = destination
	this.owner = owner
	this.group = group

	this.game.world.FleetMgr.IncShipCounter(group)

	//position ship
	pos := origin.GetPosition()
	r := origin.radius + this.radius + rand.Float32()*8
	angle := math32.Atan2(origin.GetPosition().Y-destination.GetPosition().Y, origin.GetPosition().X-destination.GetPosition().X)
	angle += rand.Float32()*math32.Pi/4 - math32.Pi/8

	this.position = pos.Minus(Vector2f{math32.Cos(angle) * r, math32.Sin(angle) * r})
	this.sprite.SetPosition(sf.Vector2f(this.position))

	this.rotation = float32(angle)
	this.sprite.SetRotation(RadToDeg(this.rotation))

	this.velocity = Vector2f{math32.Cos(angle), math32.Sin(angle)}

	//set active
	this.active = true
	this.visible = true
}

func (this *Ship) Sleep() {
	this.active = false
	this.visible = false
}

func (this *Ship) GetLayer() int {
	return 3
}

func (this *Ship) Draw(target sf.RenderTarget, _ time.Duration) {
	target.Draw(this.sprite, sf.DefaultRenderStates())
}

func (this *Ship) GetPosition() Vector2f {
	return this.position
}

func (this *Ship) GetVelocity() Vector2f {
	return this.velocity
}

func (this *Ship) SetVelocity(vel Vector2f) {
	this.velocity = vel
}

func (this *Ship) IsStatic() bool {
	return false
}

func (this *Ship) StartSimulation(time time.Duration) {
	if this.active {
		n := this.GetPosition().Minus(this.destination.GetPosition())
		l := n.GetLength()

		n.X /= l
		n.Y /= l

		this.velocity = Vector2f{n.X * SHIP_SPEED * float32(time.Seconds()), n.Y * SHIP_SPEED * float32(time.Seconds())}
	}
}

func (this *Ship) BoundingBox() sf.FloatRect {
	return sf.FloatRect{Left: this.position.X - this.radius, Top: this.position.Y - this.radius, Width: 2 * this.radius, Height: 2 * this.radius}
}

func (this *Ship) EndSimulation() {
	if this.active {

		//update position
		this.position = this.GetPosition().Minus(this.velocity)
		this.sprite.SetPosition(sf.Vector2f(this.position))

		//rotate sprite to movement direction
		this.rotation = Slerp(this.rotation, math32.Atan2(this.velocity.Y, this.velocity.X), SHIP_ROT_SMOOTH)

		this.sprite.SetRotation(RadToDeg(this.rotation))

		//check destination
		l := (this.GetPosition().Minus(this.destination.GetPosition())).GetLengthSQ()
		r := this.destination.GetRadius() + this.GetRadius() + 3
		if l < r*r {
			//do collision stuff
			this.destination.ShipImpact(this.owner)

			//let the fleetmgr know
			this.game.world.FleetMgr.DecShipCounter(this.group)

			//create effects
			this.game.effectPool.Pop().(*SplashEffect).Setup(this.GetPosition(), sf.ColorWhite(), SplashTexture1, 50, 150, 100*time.Millisecond)

			//put the ship back in the pool and let it sleep
			this.Sleep()
			this.game.shipPool.Push(this)
		}
	}
}

func (this *Ship) GetRadius() float32 {
	return this.radius
}

func (this *Ship) IsVisible() bool {
	return this.visible
}

func (this *Ship) IsActive() bool {
	return this.active
}

func (this *Ship) GetGroup() int {
	return this.group
}
