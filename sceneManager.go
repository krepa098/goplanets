// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// CONSTS
//////////////////////

const (
	LAYER_COUNT = 10
	FOREGROUND  = 0
	BACKGROUND  = 1
)

//////////////////////
// STRUCTS
//////////////////////

type PostProcessingData struct {
	rs             sf.RenderStates
	useBackground  bool
	useForeground  bool
	useTextureSize bool
}

type SceneManager struct {
	renderWindow *sf.RenderWindow
	layers       [LAYER_COUNT]SceneNodes

	//post processing
	renderTextures     [2]*sf.RenderTexture
	postProcessingData [LAYER_COUNT]PostProcessingData
	foregroundSprite   *sf.Sprite

	//benchmark info
	drawingStart time.Time
	drawingDelta time.Duration
}

//////////////////////
// FUNCS
//////////////////////

func NewSceneManager(wnd *sf.RenderWindow) *SceneManager {
	smgr := &SceneManager{}
	smgr.renderWindow = wnd

	smgr.renderTextures[0], _ = sf.NewRenderTexture(wnd.GetSize().X, wnd.GetSize().Y, false)
	smgr.renderTextures[1], _ = sf.NewRenderTexture(wnd.GetSize().X, wnd.GetSize().Y, false)
	smgr.renderTextures[0].SetSmooth(true)
	smgr.renderTextures[1].SetSmooth(true)

	smgr.foregroundSprite, _ = sf.NewSprite(nil)

	for i := 0; i < LAYER_COUNT; i++ {
		smgr.postProcessingData[i].rs = sf.DefaultRenderStates()
	}

	//shaders
	shader, _ := sf.NewShaderFromFile("", "resources/shaders/refraction.glsl")
	smgr.postProcessingData[4] = PostProcessingData{sf.RenderStates{sf.BlendAlpha, sf.TransformIdentity(), shader, nil}, true, true, true}

	shader, _ = sf.NewShaderFromFile("", "resources/shaders/alphaMerge.glsl")
	smgr.postProcessingData[2] = PostProcessingData{sf.RenderStates{sf.BlendAlpha, sf.TransformIdentity(), shader, nil}, false, true, false}

	return smgr
}

func (this *SceneManager) ResizeRenderTargets(wnd *sf.RenderWindow) {
	this.renderTextures[0], _ = sf.NewRenderTexture(wnd.GetSize().X, wnd.GetSize().Y, false)
	this.renderTextures[1], _ = sf.NewRenderTexture(wnd.GetSize().X, wnd.GetSize().Y, false)
}

func (this *SceneManager) AddSceneNode(node SceneNode) {
	this.layers[node.GetLayer()] = append(this.layers[node.GetLayer()], node)
}

func (this *SceneManager) ClearScene() {
	for i := 0; i < LAYER_COUNT; i++ {
		this.layers[i] = this.layers[i][:0]
	}
}

func (this *SceneManager) RemoveSceneNode(node SceneNode) {
	for i, e := range this.layers[node.GetLayer()] {
		if e == node {
			this.layers[node.GetLayer()] = append(this.layers[node.GetLayer()][:i], this.layers[node.GetLayer()][i+1:]...)
			break
		}
	}
}

func (this *SceneManager) DrawAll(target sf.RenderTarget, delta time.Duration) {
	this.drawingStart = time.Now()

	this.renderTextures[0].SetView(target.GetView())
	this.renderTextures[1].SetView(target.GetView())

	this.renderTextures[BACKGROUND].Clear(sf.ColorTransparent())
	this.renderTextures[FOREGROUND].Clear(sf.ColorTransparent())

	//note: only merge RT if necessary
	for l := 0; l < LAYER_COUNT; l++ {
		shader := this.postProcessingData[l].rs.Shader

		//draw sceneNodes
		for _, elem := range this.layers[l] {
			if elem.IsVisible() {
				if shader == nil {
					elem.Draw(this.renderTextures[BACKGROUND], delta) //if no merge-shader is set -> draw directly to the background-rt
				} else {
					elem.Draw(this.renderTextures[FOREGROUND], delta) //we have a shader -> draw on foreground, then merge
				}
			}
		}

		if shader != nil {
			//set shader params

			if this.postProcessingData[l].useBackground {
				shader.SetTextureParameter("texture1", this.renderTextures[BACKGROUND].GetTexture())
			}
			if this.postProcessingData[l].useForeground {
				shader.SetTextureParameter("texture0", this.renderTextures[FOREGROUND].GetTexture())
			}
			if this.postProcessingData[l].useTextureSize {
				shader.SetFloatParameter("textureSize", float32(this.renderTextures[0].GetSize().X), float32(this.renderTextures[0].GetSize().Y))
			}

			//merge using the post-processing shader
			this.MergeRenderTextures(&this.postProcessingData[l].rs)
		}
	}

	this.renderTextures[BACKGROUND].Display()
	this.foregroundSprite.SetTexture(this.renderTextures[BACKGROUND].GetTexture(), true)

	oldview := target.GetView()
	target.SetView(target.GetDefaultView())
	this.foregroundSprite.Draw(target, sf.DefaultRenderStates())
	target.SetView(oldview)

	this.drawingDelta = time.Since(this.drawingStart)
}

func (this *SceneManager) MergeRenderTextures(rs *sf.RenderStates) {
	oldView := this.renderTextures[BACKGROUND].GetView()

	this.renderTextures[FOREGROUND].Display()
	this.foregroundSprite.SetTexture(this.renderTextures[FOREGROUND].GetTexture(), false)
	this.renderTextures[BACKGROUND].SetView(this.renderTextures[BACKGROUND].GetDefaultView())
	this.renderTextures[BACKGROUND].Draw(this.foregroundSprite, *rs)
	this.renderTextures[FOREGROUND].Clear(sf.ColorTransparent())

	this.renderTextures[BACKGROUND].SetView(oldView)
}
