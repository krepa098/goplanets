// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	gui "bitbucket.org/krepa098/acropolis"
	sf "bitbucket.org/krepa098/gosfml2"
)

//////////////////////
// STRUCTS
//////////////////////

type ViewData struct {
	l, t, w, h float32 //viewport
	cx, cy     float32 //center
	sx, sy     float32 //size
}

type ClipStack []ViewData

//////////////////////
// VARS
//////////////////////

var clipStack ClipStack

//////////////////////
// FUNCS
//////////////////////

func findWidget(screen *gui.Screen, name string) gui.Widgeter {
	if results := screen.FindChildren(name, true); len(results) > 0 {
		return results[0]
	}
	return nil
}

func (this *ClipStack) Empty() bool {
	return len(*this) == 0
}

func (this *ClipStack) PushData(data ViewData) {
	*this = append(*this, data)
}

func (this *ClipStack) PopData() (data ViewData) {
	if !this.Empty() {
		data = (*this)[len(*this)-1]
		*this = (*this)[:len(*this)-1]
	}
	return
}

func (this *ClipStack) Front() (data ViewData) {
	if !this.Empty() {
		data = (*this)[0]
	}
	return
}

func (this *ClipStack) ClipArea(target sf.RenderTarget, area gui.Geometry) {
	cvd := ViewData{}

	cvd.l = target.GetView().GetViewport().Left
	cvd.t = target.GetView().GetViewport().Top
	cvd.w = target.GetView().GetViewport().Width
	cvd.h = target.GetView().GetViewport().Height
	cvd.sx = target.GetView().GetSize().X
	cvd.sy = target.GetView().GetSize().Y
	cvd.cx = target.GetView().GetCenter().X
	cvd.cy = target.GetView().GetCenter().Y

	//save current view state
	this.PushData(cvd)

	cvd = this.Front()

	viewWidth := cvd.sx
	viewHeight := cvd.sy

	vd := ViewData{}
	vd.t = float32(area.Top) / viewHeight
	vd.l = float32(area.Left) / viewWidth
	vd.w = float32(area.Width) / viewWidth
	vd.h = float32(area.Height) / viewHeight

	vd.cx = (vd.l + vd.w/2) * viewWidth
	vd.cy = (vd.t + vd.h/2) * viewHeight

	vd.sx = viewWidth * vd.w
	vd.sy = viewHeight * vd.h

	//apply new view state
	this.ApplyViewData(vd, target)
}

func (this *ClipStack) UnclipArea(target sf.RenderTarget) {
	if this.Empty() {
		return
	}

	//load back old view state
	data := this.PopData()
	this.ApplyViewData(data, target)
}

func (this *ClipStack) ApplyViewData(vd ViewData, target sf.RenderTarget) {
	view := target.GetView()
	view.SetViewport(sf.FloatRect{vd.l, vd.t, vd.w, vd.h})
	view.SetSize(sf.Vector2f{vd.sx, vd.sy})
	view.SetCenter(sf.Vector2f{vd.cx, vd.cy})
	target.SetView(view)
}
