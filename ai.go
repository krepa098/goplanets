// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
	"math/rand"
	"sort"
	"time"
)

//////////////////////
// CONSTS
//////////////////////

const (
	AI_PROXIMITYCOEFF = 10000
	AI_MIN_THINKTIME  = 2000 //ms
	AI_THINKTIME      = 1500 //ms
)

//////////////////////
// STRUCTS
//////////////////////

type PlanetData struct {
	planet *Planet
	score  float32
}

type ScoreList []PlanetData

type descending ScoreList

func (this descending) Len() int {
	return len(this)
}

func (this descending) Less(i, j int) bool {
	return this[i].score > this[j].score
}

func (this descending) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

type ascending ScoreList

func (this ascending) Len() int {
	return len(this)
}

func (this ascending) Less(i, j int) bool {
	return this[i].score < this[j].score
}

func (this ascending) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

type AIPlayer struct {
	game     *Game
	color    sf.Color
	timeAccu time.Duration
	nextTurn time.Duration
	name     string

	//scoring
	strengthScoreList ScoreList
	targetScoreList   ScoreList
	defenseScoreList  ScoreList
}

//////////////////////
// FUNCS
//////////////////////

func NewAIPlayer(game *Game, color sf.Color) *AIPlayer {
	player := new(AIPlayer)
	player.nextTurn = (AI_MIN_THINKTIME + time.Duration(rand.Int()%AI_THINKTIME)) * time.Millisecond

	player.color = color
	player.game = game

	return player
}

func (this *AIPlayer) GetName() string {
	return this.name
}

func (this *AIPlayer) SetName(name string) {
	this.name = name
}

func (this *AIPlayer) GetColor() sf.Color {
	return this.color
}

func (this *AIPlayer) AIControlled() bool {
	return true
}

func (this *AIPlayer) Update(dt time.Duration) {

	this.timeAccu += dt

	if this.timeAccu < this.nextTurn {
		return
	}

	this.nextTurn = (AI_MIN_THINKTIME + time.Duration(rand.Int()%AI_THINKTIME)) * time.Millisecond
	this.timeAccu = 0

	this.gatherData()

	switch rand.Int() % 2 {
	case 0:
		this.Defend()
	case 1:
		this.Attack()
	}
}

func (this *AIPlayer) gatherData() {
	//resize
	if len(game.world.Planets) != len(this.strengthScoreList) {
		this.strengthScoreList = make(ScoreList, len(game.world.Planets))
		this.defenseScoreList = make(ScoreList, len(game.world.Planets))
		this.targetScoreList = make(ScoreList, len(game.world.Planets))

		//copy
		for i, p := range game.world.Planets {
			this.strengthScoreList[i].planet = p
			this.defenseScoreList[i].planet = p
			this.targetScoreList[i].planet = p
		}
	}

	j := 0

	//calc scores (source planet)
	this.strengthScoreList = this.strengthScoreList[:cap(this.strengthScoreList)] //regrow
	for i := range this.strengthScoreList {
		if this.strengthScoreList[i].planet.owner == this {
			this.strengthScoreList[i].score = float32(this.strengthScoreList[i].planet.ships)
			j++
		} else {
			this.strengthScoreList[i].score = float32(math32.Inf(-1)) //to be removed from list
		}
	}

	sort.Sort(descending(this.strengthScoreList))
	this.strengthScoreList = this.strengthScoreList[:j]

	j = 0
	//calc scores (target)
	this.targetScoreList = this.targetScoreList[:cap(this.targetScoreList)] //regrow
	for i := range this.targetScoreList {
		if this.targetScoreList[i].planet.owner != this {
			this.targetScoreList[i].score = float32(this.targetScoreList[i].planet.ships + this.game.world.FleetMgr.GetShipsAttackingPlanet(this.targetScoreList[i].planet)*4)
			j++
		} else {
			this.targetScoreList[i].score = math32.Inf(1) //to be removed from list
		}
	}
	sort.Sort(ascending(this.targetScoreList))
	this.targetScoreList = this.targetScoreList[:j]

	j = 0
	//calc scores (defense)
	this.defenseScoreList = this.defenseScoreList[:cap(this.defenseScoreList)] //regrow
	for i := range this.defenseScoreList {
		if this.defenseScoreList[i].planet.owner == this && this.game.world.FleetMgr.GetShipsAttackingPlanet(this.defenseScoreList[i].planet) > this.defenseScoreList[i].planet.Ships() {
			this.defenseScoreList[i].score = float32(this.game.world.FleetMgr.GetShipsAttackingPlanet(this.defenseScoreList[i].planet)) * this.defenseScoreList[i].planet.radius
			j++
		} else {
			this.defenseScoreList[i].score = math32.Inf(-1) //to be removed from list
		}
	}
	sort.Sort(descending(this.defenseScoreList))
	this.defenseScoreList = this.defenseScoreList[:j]
}

func (this *AIPlayer) Defend() {
	if len(this.defenseScoreList) == 0 || len(this.strengthScoreList) == 0 {
		return
	}
	var (
		weakestPlanet = this.defenseScoreList[0].planet
		shipsRequired = 0 //weakestPlanet.incomingEnemyShips + 5
		ok            = false
		defCoeff      = float32(0)
		defCoeffs     = [...]float32{0.1, 0.3, 0.5, 0.6, 0.9}
		c             = 0
		gatheredShips = 0
	)

	//try to gather enough ships to defend it
	for _, coeff := range defCoeffs {
		gatheredShips = 0
		c = 0
		for _, data := range this.strengthScoreList {
			if data.planet == weakestPlanet {
				continue
			}

			gatheredShips += int(float32(data.planet.ships) * coeff)
			c++

			if gatheredShips > shipsRequired {
				ok = true
				defCoeff = coeff
				break
			}
		}
	}

	if ok {
		for i := 0; i < c; i++ {
			if this.strengthScoreList[i].planet == weakestPlanet {
				continue
			}
			this.strengthScoreList[i].planet.SendShips(defCoeff, weakestPlanet)
		}
	} else {
		//fmt.Println("Not defending")
	}

}

func (this *AIPlayer) Attack() {
	if len(this.targetScoreList) == 0 || len(this.strengthScoreList) == 0 {
		return
	}

	var (
		bestTarget    = this.targetScoreList[0].planet
		shipsRequired = bestTarget.ships
		c             = 0
		gatheredShips = 0
		ok            = false //wise move?
		attackCoeff   = float32(0)
		attackCoeffs  = [...]float32{0.1, 0.2, 0.3, 0.5, 0.6}
	)

	//try to gather enough ships to take it over
	for _, coeff := range attackCoeffs {
		gatheredShips = 0
		c = 0
		for _, data := range this.strengthScoreList {
			d := data.planet.GetPosition().Minus(bestTarget.GetPosition()).GetLength()
			penalty := int(d / (SHIP_SPEED * (float32(this.targetScoreList[0].planet.GetRefillRate().Seconds()))))
			strength := int(float32(data.planet.Ships()) * coeff)

			if strength-penalty <= 0 {
				strength = 0
			} else {
				strength -= penalty
			}

			gatheredShips += strength
			c++

			if gatheredShips > shipsRequired {
				ok = true
				attackCoeff = coeff
				break
			}
		}
	}

	if ok {
		for i := 0; i < c; i++ {
			this.strengthScoreList[i].planet.SendShips(attackCoeff, bestTarget)
		}
	} else {
		//fmt.Println("Not attacking")
	}
}
