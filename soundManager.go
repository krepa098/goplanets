// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// CONSTS
//////////////////////

const (
	SOUND_TICK_INTERVAL     = time.Second / 5
	FADE_SPEED              = 140
	MAX_SIMULTANIOUS_SOUNDS = 64
)

//////////////////////
// INTERFACES
//////////////////////

//sf.Music/sf.Sound
type Fader interface {
	SetVolume(float32)
	GetVolume() float32
	Stop()
	Play()
}

//////////////////////
// STRUCTS
//////////////////////

type FadeTask struct {
	sound  Fader
	fadeIn bool
}

type SoundManager struct {
	musics       []*sf.Music
	soundbuffers map[string]*sf.SoundBuffer
	sounds       [MAX_SIMULTANIOUS_SOUNDS]*sf.Sound

	musicLoopFrom, musicLoopTo, musicCurrentIndex int

	fadeTicker *time.Ticker
	tasks      []FadeTask
}

//////////////////////
// FUNCS
//////////////////////

func NewSoundManager() *SoundManager {
	smgr := &SoundManager{}

	//init vars
	smgr.soundbuffers = make(map[string]*sf.SoundBuffer)
	smgr.musicLoopFrom, smgr.musicLoopTo = -1, -1

	//init sounds
	for i := 0; i < len(smgr.sounds); i++ {
		smgr.sounds[i] = sf.NewSound(nil)
	}

	//ticker
	smgr.fadeTicker = time.NewTicker(SOUND_TICK_INTERVAL)

	smgr.musicCurrentIndex = -1

	return smgr
}

func (this *SoundManager) Update() {
	//handle tasks
	select {
	case <-this.fadeTicker.C:
		for i, t := range this.tasks {
			if t.fadeIn {
				v := t.sound.GetVolume()

				v += FADE_SPEED * (float32(SOUND_TICK_INTERVAL) / float32(time.Second))
				t.sound.SetVolume(v)

				if v >= 100 { //done
					t.sound.SetVolume(100)
					this.tasks[i], this.tasks = this.tasks[len(this.tasks)-1], this.tasks[:len(this.tasks)-1] //remove task
					break
				}
			} else {
				v := t.sound.GetVolume()
				v -= FADE_SPEED * (float32(SOUND_TICK_INTERVAL) / float32(time.Second))
				t.sound.SetVolume(v)

				if v <= 0 { //done
					t.sound.Stop()
					this.tasks[i], this.tasks = this.tasks[len(this.tasks)-1], this.tasks[:len(this.tasks)-1] //remove task
					break
				}
			}
		}
	default:
	}

	if this.musicCurrentIndex >= 0 {
		if this.musics[this.musicCurrentIndex].GetStatus() == sf.SoundStatusStopped {
			this.musicCurrentIndex++
			if this.musicCurrentIndex > this.musicLoopTo {
				this.musicCurrentIndex = this.musicLoopFrom
			}

			this.CmdFadeIn(this.musics[this.musicCurrentIndex])
		}
	}

}

func (this *SoundManager) LoadSound(soundfile string) {
	sb, _ := sf.NewSoundBufferFromFile(soundfile)
	this.soundbuffers[soundfile] = sb
}

func (this *SoundManager) PlaySound(soundfile string) {
	for i := 0; i < len(this.sounds); i++ {
		if this.sounds[i].GetStatus() == sf.SoundStatusStopped {
			this.sounds[i].SetBuffer(this.soundbuffers[soundfile])
			this.sounds[i].Play()
			break
		}
	}
}

func (this *SoundManager) LoadMusic(soundfile string) {
	music, _ := sf.NewMusicFromFile(soundfile)
	this.musics = append(this.musics, music)
}

func (this *SoundManager) LoopMusic(from, to int) {
	if this.musicLoopFrom == from && this.musicLoopTo == to {
		return
	}

	this.musicLoopFrom, this.musicLoopTo = from, to

	//stop current music
	if this.musicCurrentIndex >= 0 {
		this.CmdFadeOut(this.musics[this.musicCurrentIndex])
	}

	//start new music
	this.musicCurrentIndex = from
	this.CmdFadeIn(this.musics[this.musicCurrentIndex])
}

func (this *SoundManager) MusicCount() int {
	return len(this.musics)
}

func (this *SoundManager) CmdFadeIn(f Fader) {
	this.tasks = append(this.tasks, FadeTask{f, true})
	f.SetVolume(0)
	f.Play()
}

func (this *SoundManager) CmdFadeOut(f Fader) {
	this.tasks = append(this.tasks, FadeTask{f, false})
}
