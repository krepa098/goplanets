// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

//////////////////////
// STRUCTS
//////////////////////

type Fleet struct {
	Source, Destination *Planet
	Owner               Player
	Ships               int
	ShipCounter         int
	Dead                bool
}

type FleetManager struct {
	Fleets []Fleet
}

//////////////////////
// FUNCS
//////////////////////

func (this *FleetManager) AddFleet(f Fleet) int {
	//search for an empty/unused slot
	for i := 0; i < len(this.Fleets); i++ {
		if this.Fleets[i].Dead {
			this.Fleets[i] = f
			return i
		}
	}

	this.Fleets = append(this.Fleets, f)
	return len(this.Fleets) - 1
}

func (this *FleetManager) Clear() {
	this.Fleets = this.Fleets[:0]
}

func (this *FleetManager) GetShipsOwnedByPlayer(p Player) (c int) {
	for i := 0; i < len(this.Fleets); i++ {
		if this.Fleets[i].Owner == p && !this.Fleets[i].Dead {
			c += this.Fleets[i].ShipCounter
		}
	}
	return
}

func (this *FleetManager) GetGlobalShipCount() (c int) {
	for i := 0; i < len(this.Fleets); i++ {
		if !this.Fleets[i].Dead {
			c += this.Fleets[i].ShipCounter
		}
	}
	return
}

func (this *FleetManager) DecShipCounter(fleetId int) {
	if fleetId >= 0 && fleetId < len(this.Fleets) && this.Fleets[fleetId].ShipCounter > 0 {
		this.Fleets[fleetId].ShipCounter--
		if this.Fleets[fleetId].ShipCounter <= 0 {
			this.Fleets[fleetId].Dead = true
		}
	}
}

func (this *FleetManager) IncShipCounter(fleetId int) {
	if fleetId >= 0 && fleetId < len(this.Fleets) {
		this.Fleets[fleetId].ShipCounter++
	}
}

func (this *FleetManager) GetShipsAttackingPlanet(p *Planet) (c int) {
	for i := 0; i < len(this.Fleets); i++ {
		if this.Fleets[i].Owner != p.owner && this.Fleets[i].Destination == p && !this.Fleets[i].Dead {
			c += this.Fleets[i].Ships
		}
	}
	return
}
