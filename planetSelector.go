// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
	"time"
)

//////////////////////
// VARS
//////////////////////

var (
	SelectionFillColor = sf.Color{0, 0, 0, 255}
)

//////////////////////
// CONSTS
//////////////////////

const (
	OUTLINE_TICKNESS = 4
)

//////////////////////
// STRUCTS
//////////////////////

type PlanetSelector struct {
	selectedPlanets   []*Planet
	destinationPlanet *Planet
	lastPlanet        *Planet
	owner             Player
	game              *Game

	//visuals
	circleShape *sf.CircleShape
	rectShape   *sf.RectangleShape
}

//////////////////////
// FUNCS
//////////////////////

func NewPlanetSelector(game *Game, owner Player) *PlanetSelector {
	selector := new(PlanetSelector)
	selector.owner = owner
	selector.game = game

	selector.circleShape, _ = sf.NewCircleShape()
	selector.circleShape.SetPointCount(64)
	selector.rectShape, _ = sf.NewRectangleShape()

	game.smgr.AddSceneNode(selector)

	return selector
}

func (this *PlanetSelector) GetLayer() int {
	return 2
}

func (this *PlanetSelector) Draw(target sf.RenderTarget, _ time.Duration) {
	this.circleShape.SetFillColor(sf.ColorTransparent())
	this.circleShape.SetOutlineColor(SelectionFillColor)
	this.circleShape.SetOutlineThickness(OUTLINE_TICKNESS)

	if this.destinationPlanet != nil {
		this.circleShape.SetRadius(this.destinationPlanet.radius)
		this.circleShape.SetOrigin(sf.Vector2f{this.destinationPlanet.radius, this.destinationPlanet.radius})
		this.circleShape.SetPosition(sf.Vector2f(this.destinationPlanet.GetPosition()))

		target.Draw(this.circleShape, sf.DefaultRenderStates())
	}

	//draw circles around selected planets + travel lines
	for _, e := range this.selectedPlanets {
		this.circleShape.SetRadius(e.radius)
		this.circleShape.SetOrigin(sf.Vector2f{e.radius, e.radius})
		this.circleShape.SetPosition(sf.Vector2f(e.GetPosition()))

		target.Draw(this.circleShape, sf.DefaultRenderStates())

		//draw travel lines
		//calculate some values
		n := this.GetDestinationPlanet().GetPosition().Minus(e.GetPosition())
		l := n.GetLength() - this.GetDestinationPlanet().GetRadius() - e.GetRadius() - 2*OUTLINE_TICKNESS

		this.rectShape.SetSize(sf.Vector2f{l, OUTLINE_TICKNESS})
		this.rectShape.SetFillColor(SelectionFillColor)
		this.rectShape.SetOutlineColor(SelectionFillColor)
		this.rectShape.SetOutlineThickness(1)
		this.rectShape.SetOrigin(sf.Vector2f{0, this.rectShape.GetLocalBounds().Height / 2})

		angle := math32.Atan2(n.Y, n.X)
		this.rectShape.SetPosition(sf.Vector2f{math32.Cos(angle)*(e.GetRadius()+OUTLINE_TICKNESS) + e.GetPosition().X, math32.Sin(angle)*(e.GetRadius()+OUTLINE_TICKNESS) + e.GetPosition().Y})
		this.rectShape.SetRotation(RadToDeg(angle))

		target.Draw(this.rectShape, sf.DefaultRenderStates())
	}

}

func (this *PlanetSelector) SetPlanets(planet *Planet) {
	if planet == nil || this.lastPlanet == planet {
		return
	} else {
		this.lastPlanet = planet
	}

	in, index := this.AlreadyInList(planet)

	if in {
		//new destination
		this.destinationPlanet = planet
		//remove from source planets
		this.selectedPlanets = append(this.selectedPlanets[:index], this.selectedPlanets[index+1:]...)
		return
	}

	if this.destinationPlanet != nil {
		if this.destinationPlanet.owner == this.owner {
			this.selectedPlanets = append(this.selectedPlanets, this.destinationPlanet)
		}
	}

	this.destinationPlanet = planet

}

func (this *PlanetSelector) AlreadyInList(planet *Planet) (in bool, index int) {
	for i, e := range this.selectedPlanets {
		if e == planet {
			return true, i
		}
	}
	return false, -1
}

func (this *PlanetSelector) Reset() {
	this.selectedPlanets = nil
	this.destinationPlanet = nil
	this.lastPlanet = nil
}

func (this *PlanetSelector) GetPlanets() (sourcePlanets []*Planet, destinationPlanet *Planet, valid bool) {
	return this.selectedPlanets, this.destinationPlanet, len(this.selectedPlanets) > 0 && this.destinationPlanet != nil
}

func (this *PlanetSelector) GetDestinationPlanet() *Planet {
	return this.destinationPlanet
}

func (this *PlanetSelector) IsVisible() bool {
	return true
}
