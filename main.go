// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"flag"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
)

var (
	//cmd
	w            = flag.Int("width", 1024, "window width")
	h            = flag.Int("height", 768, "window height")
	fullscreen   = flag.Bool("fullscreen", false, "fullscreen=true uses the native desktop resolution")
	vsync        = flag.Bool("vsync", true, "vsync=true to enable vsync")
	cpuprofile   = flag.String("cpuprofile", "", "write cpu profile to file")
	blockprofile = flag.String("blockprofile", "", "write block profile to file")
	//
	game *Game
)

func init() {
	runtime.LockOSThread()
	runtime.GOMAXPROCS(CPU_CORES * 2)
}

func main() {
	flag.Parse()

	//Profiling
	if *blockprofile != "" {
		runtime.SetBlockProfileRate(1)
	}

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}

		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	//Adjust volume
	sf.ListenerSetGlobalVolume(70)

	//Resolution
	if *fullscreen {
		game = NewGame(sf.GetDesktopVideoMode().Width, sf.GetDesktopVideoMode().Height, true, *vsync)
	} else {
		game = NewGame(uint(*w), uint(*h), false, *vsync)
	}

	//Go!
	game.Run()

	//Profiling
	if *blockprofile != "" {
		fb, _ := os.Create(*blockprofile)
		pprof.Lookup("block").WriteTo(fb, 0)
		fb.Close()
	}
}
