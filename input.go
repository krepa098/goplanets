// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"time"
)

//////////////////////
// STRUCTS
//////////////////////

type InputManager struct {
	mousePosition   sf.Vector2i
	mouseWheelDelta int
	doubleClicked   bool
	hasFocus        bool
	lastClick       time.Time
}

//////////////////////
// FUNCS
//////////////////////

func NewInputManager() *InputManager {
	return &InputManager{hasFocus: true}
}

func (this *InputManager) HandleEvent(event sf.Event) {
	switch ev := event.(type) {
	case sf.EventMouseMoved:
		this.mousePosition.X = ev.X
		this.mousePosition.Y = ev.Y
	case sf.EventMouseWheelMoved:
		this.mouseWheelDelta = ev.Delta
	case sf.EventLostFocus:
		this.hasFocus = false
	case sf.EventGainedFocus:
		this.hasFocus = true
	case sf.EventMouseButtonPressed:
		if ev.Button == sf.MouseLeft {
			if time.Now().Sub(this.lastClick) <= 400*time.Millisecond {
				this.doubleClicked = true
			}
			this.lastClick = time.Now()
		}
	}
}

func (this *InputManager) NewFrame() {
	this.mouseWheelDelta = 0

	if this.doubleClicked {
		this.doubleClicked = false
	}
}

func (this *InputManager) IsKeyDown(key sf.KeyCode) bool {
	return sf.KeyboardIsKeyPressed(key) && this.hasFocus
}

func (this *InputManager) IsMouseButtonDown(key sf.MouseButton) bool {
	return sf.IsMouseButtonPressed(key) && this.hasFocus
}
