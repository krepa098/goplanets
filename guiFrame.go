// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
)

//////////////////////
// STRUCTS
//////////////////////

type GuiFrame struct {
	vertexArray                 *sf.VertexArray
	transform                   sf.Transform
	width, height, cornerRadius float32
}

const EDGE_DIV = 4

//////////////////////
// FUNCS
//////////////////////

func NewGuiFrame() *GuiFrame {
	guiFrame := new(GuiFrame)
	guiFrame.vertexArray, _ = sf.NewVertexArray()
	guiFrame.vertexArray.PrimitiveType = sf.PrimitiveTrianglesFan
	guiFrame.transform = sf.TransformIdentity()

	return guiFrame
}

func (this *GuiFrame) Draw(target sf.RenderTarget) {
	//Draw
	target.Draw(this.vertexArray, sf.RenderStates{sf.BlendAlpha, this.transform, nil, nil})
}

func (this *GuiFrame) Generate(width, height, cornerRadius float32, color sf.Color) {
	this.width, this.height, this.cornerRadius = width, height, cornerRadius

	//smallest stride
	s := math32.Min(width, height)
	cornerRadius *= s / 2

	//clear old data
	this.vertexArray.Clear()

	//corners
	cornerTopLeft := sf.Vector2f{cornerRadius, cornerRadius}
	cornerTopRight := sf.Vector2f{width - cornerRadius, cornerRadius}
	cornerBottomLeft := sf.Vector2f{cornerRadius, height - cornerRadius}
	cornerBottomRight := sf.Vector2f{width - cornerRadius, height - cornerRadius}

	//add fan pivot
	this.vertexArray.Append(sf.Vertex{Position: sf.Vector2f{width / 2, height / 2}, Color: color})

	//generate geometry
	//top right
	this.GenerateCorner(cornerTopRight, 0, math32.Pi/2, cornerRadius, color)

	//top left
	this.GenerateCorner(cornerTopLeft, math32.Pi/2, math32.Pi, cornerRadius, color)

	//bottom left
	this.GenerateCorner(cornerBottomLeft, math32.Pi, 3*math32.Pi/2, cornerRadius, color)

	//bottom right
	this.GenerateCorner(cornerBottomRight, 3*math32.Pi/2, math32.Pi*2, cornerRadius, color)
	this.vertexArray.Append(sf.Vertex{Position: sf.Vector2f{width, height - cornerRadius}, Color: color})
	this.vertexArray.Append(sf.Vertex{Position: sf.Vector2f{width, cornerRadius}, Color: color})
}

func (this *GuiFrame) SetPosition(pos sf.Vector2f) {
	this.transform = sf.TransformIdentity()
	this.transform.Translate(pos.X, pos.Y)
}

func (this *GuiFrame) SetColor(color sf.Color) {
	for i := 0; i < this.vertexArray.GetVertexCount(); i++ {
		this.vertexArray.Vertices[i].Color = color
	}
}

func (this *GuiFrame) GenerateCorner(pivot sf.Vector2f, from, to, radius float32, color sf.Color) {
	for f := from; f <= to; f += (to - from) / EDGE_DIV {
		x := math32.Cos(f) * radius
		y := math32.Sin(f) * radius

		this.vertexArray.Append(sf.Vertex{Position: pivot.Plus(sf.Vector2f{x, -y}), Color: color})
	}

}
