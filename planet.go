// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"github.com/AE9RB/math32"
	"math/rand"
	"strconv"
	"time"
)

//////////////////////
// VARS
//////////////////////

var (
	PlanetTextures = [...]*sf.Texture{
		LoadTexture("resources/gfx/planet1.png"),
		LoadTexture("resources/gfx/planet2.png")}

	PlanetGlowTexture = LoadTexture("resources/gfx/planet_glow.png")

	OrbitFillColor    = sf.Color{0, 0, 0, 0}
	OrbitOutlineColor = sf.Color{255, 255, 255, 50}

	PLANET_REFILL = time.Duration(DEFAULT_PLANET_REFILL)
)

const (
	SHIP_SPAWN_RATE  = 25 * time.Millisecond
	PLANET_ROT_SPEED = 0.0005
)

//////////////////////
// STRUCTS
//////////////////////

type Command struct {
	destination *Planet
	amount      int
	group       int
}

type Planet struct {
	sprite        *sf.Sprite
	glowSprite    *sf.Sprite
	orbitSprite   *sf.CircleShape
	text          *sf.Text
	game          *Game
	radius        float32
	ships         int
	outgoingShips int
	owner         Player
	refillAccu    time.Duration
	spawnAccu     time.Duration
	refillRate    time.Duration
	position      Vector2f

	orbitSpeed    float32 //rad/s
	orbitRadius   float32
	orbitValue    float32
	rotation      float32
	rotationSpeed float32
	commandQeue   []Command
	parent        PhysicalEntity
}

//////////////////////
// FUNCS
//////////////////////

func NewPlanet(game *Game, pos Vector2f, radius float32, owner Player) *Planet {
	planet := new(Planet)

	//visual
	sprite, _ := sf.NewSprite(PlanetTextures[rand.Int()%len(PlanetTextures)])
	sprite.SetPosition(sf.Vector2f(pos))
	sprite.SetRotation(rand.Float32() * 360)

	size := Vector2f{sprite.GetLocalBounds().Width, sprite.GetLocalBounds().Height}
	sprite.SetOrigin(sf.Vector2f{size.X / 2, size.Y / 2})
	sprite.SetScale(sf.Vector2f{radius * 2 / size.X, radius * 2 / size.Y})

	glowSprite, _ := sf.NewSprite(PlanetGlowTexture)
	glowSprite.SetPosition(sf.Vector2f(pos))
	glowSprite.SetRotation(rand.Float32() * 360)

	size = Vector2f{sprite.GetLocalBounds().Width, sprite.GetLocalBounds().Height}
	glowSprite.SetOrigin(sf.Vector2f{size.X / 2, size.Y / 2})
	glowSprite.SetScale(sf.Vector2f{radius * 4 / size.X, radius * 4 / size.Y})

	planet.sprite = sprite
	planet.glowSprite = glowSprite
	planet.radius = radius

	planet.position = pos

	//planet.rotationSpeed = PLANET_ROT_SPEED/2.0 - rand.Float32()*PLANET_ROT_SPEED
	planet.ships = int(rand.Float32() * radius)

	//owner
	planet.owner = owner

	//color
	if planet.owner != nil {
		planet.sprite.SetColor(planet.owner.GetColor())
		planet.glowSprite.SetColor(planet.owner.GetColor())
	} else {
		planet.sprite.SetColor(sf.ColorWhite())
	}

	//status text
	text, _ := sf.NewText(primitives.textFont.sfFont)
	text.SetColor(sf.ColorWhite())
	text.SetCharacterSize(18)
	text.SetPosition(sf.Vector2f(pos))

	planet.text = text

	//refill rate
	planet.refillRate = PLANET_REFILL / time.Duration(radius)

	//add to scene
	planet.game = game
	game.smgr.AddSceneNode(planet)
	game.pmgr.AddEntity(planet)

	return planet
}

func (this *Planet) BoundingBox() sf.FloatRect {
	return sf.FloatRect{Left: this.position.X - this.radius, Top: this.position.Y - this.radius, Width: 2 * this.radius, Height: 2 * this.radius}
}

func (this *Planet) Draw(target sf.RenderTarget, dt time.Duration) {
	//render orbit
	if this.orbitSprite != nil {
		this.orbitSprite.SetPosition(sf.Vector2f(this.parent.GetPosition()))
		this.orbitSprite.Draw(target, sf.RenderStates{sf.BlendAlpha, sf.TransformIdentity(), nil, nil})
	}

	if this.owner != nil {
		this.glowSprite.SetColor(this.owner.GetColor())
	}

	this.glowSprite.Draw(target, sf.DefaultRenderStates())
	this.sprite.Draw(target, sf.DefaultRenderStates())

	//update text
	sships := strconv.Itoa(this.ships)
	this.text.SetString(sships)
	this.text.SetOrigin(sf.Vector2f{this.text.GetLocalBounds().Width / 2, this.text.GetLocalBounds().Height / 2})

	//render text shadow
	this.text.SetPosition(sf.Vector2f(this.GetPosition().Plus(Vector2f{1, 2})))
	this.text.SetColor(sf.ColorBlack())
	this.text.Draw(target, sf.DefaultRenderStates())

	//render text
	this.text.SetPosition(sf.Vector2f(this.GetPosition()))
	this.text.SetColor(sf.ColorWhite())
	this.text.Draw(target, sf.DefaultRenderStates())

	//rotate planet
	this.rotation += float32(dt/time.Millisecond) * this.rotationSpeed
	this.sprite.SetRotation(RadToDeg(this.rotation))
}

func (this *Planet) GetLayer() int {
	return 1
}

func (this *Planet) IsInside(pos Vector2f) bool {
	c := pos.Minus(this.GetPosition())
	l := c.GetLength()
	return l < this.radius
}

func (this *Planet) GetPosition() Vector2f {
	return this.position
}

func (this *Planet) GetRadius() float32 {
	return this.radius
}

func (this *Planet) Covers(x, y, r float32) bool {
	c := this.GetPosition().Minus(Vector2f{x, y})
	l := c.GetLength()
	return l < this.radius+r+PLANET_SPACING
}

func (this *Planet) GetVelocity() Vector2f {
	return Vector2f{}
}

func (this *Planet) SetVelocity(vel Vector2f) {
}

func (this *Planet) IsStatic() bool {
	return true
}

func (this *Planet) EndSimulation() {
	this.sprite.SetPosition(sf.Vector2f(this.position))

	empty := true
	//handle command qeue (spawn ships)
	for this.spawnAccu >= SHIP_SPAWN_RATE {
		this.spawnAccu -= SHIP_SPAWN_RATE

		for j := range this.commandQeue {
			if this.commandQeue[j].amount > 0 && this.ships > 0 {
				empty = false

				//new ship
				ship := this.game.shipPool.Pop().(*Ship)
				ship.Setup(this, this.commandQeue[j].destination, this.owner, this.commandQeue[j].group)
				this.commandQeue[j].amount--
				this.ships--
				this.outgoingShips--
			} else {
				empty = true
			}

		}

		if empty {
			this.commandQeue = this.commandQeue[:0]
		}
	}

}

func (this *Planet) StartSimulation(time time.Duration) {
	//orbit
	if this.parent != nil {
		this.orbitValue += this.orbitSpeed * float32(time.Seconds()) / this.orbitRadius
		this.position.X = math32.Cos(this.orbitValue)*this.orbitRadius + this.parent.GetPosition().X
		this.position.Y = math32.Sin(this.orbitValue)*this.orbitRadius + this.parent.GetPosition().Y
	}

	//neutral planets produce no ships
	if this.owner == nil {
		return
	}

	this.spawnAccu += time
	this.refillAccu += time

	for this.refillAccu >= this.refillRate {
		this.ships++
		this.refillAccu -= this.refillRate
	}
}

func (this *Planet) ShipImpact(from Player) {
	if this.owner == from {
		this.ships++
	} else {
		this.ships--

		if this.ships <= 0 {
			//change owner
			this.owner = from
			//clear command qeue
			this.commandQeue = this.commandQeue[:0]
			this.outgoingShips = 0
			this.ships = 1
			//show plash effect
			this.game.effectPool.Pop().(*SplashEffect).Setup(this.GetPosition(), sf.ColorWhite(), SplashTexture2, 0, this.radius*3, 1*time.Second)
			this.game.effectPool.Pop().(*SplashEffect).Setup(this.GetPosition(), sf.ColorWhite(), SplashTexture2, this.radius/3, this.radius*2, 500*time.Millisecond)
			//change color
			this.sprite.SetColor(this.owner.GetColor())
		}
	}
}

func (this *Planet) SendShips(amountPercent float32, destination *Planet) {
	if destination == this {
		return
	}

	shipCount := int(amountPercent * float32(this.Ships()))

	//setup a fleet
	f := Fleet{Source: this, Destination: destination, Ships: shipCount, ShipCounter: 0, Owner: this.owner}
	id := this.game.world.FleetMgr.AddFleet(f)

	//issue command
	this.commandQeue = append(this.commandQeue, Command{destination: destination, amount: shipCount, group: id})
	this.outgoingShips += shipCount
}

func (this *Planet) IsVisible() bool {
	return true
}

func (this *Planet) IsActive() bool {
	return true
}

func (this *Planet) Ships() int {
	return this.ships - this.outgoingShips
}

func (this *Planet) GetGroup() int {
	return -1
}

func (this *Planet) GetRefillRate() time.Duration {
	if this.owner != nil {
		return this.refillRate
	}

	return time.Duration(math32.MaxInt32)
}
