// Copyright 2013-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"math/rand"
	"time"
)

//////////////////////
// CONSTS
//////////////////////

const (
	NEBULA_MIN_COLOR = 0.6
	NEBULA_MAX_COLOR = 1.0 - NEBULA_MIN_COLOR
)

//////////////////////
// STRUCTS
//////////////////////

type Nebula struct {
	shape  *sf.RectangleShape
	shader *sf.Shader
	time   time.Duration
}

//////////////////////
// FUNCS
//////////////////////

func NewNebula() *Nebula {
	nebula := new(Nebula)
	nebula.shape, _ = sf.NewRectangleShape()
	nebula.shader, _ = sf.NewShaderFromFile("", "resources/shaders/nebula.glsl")

	return nebula
}

func (this *Nebula) Generate(view *sf.View) {
	//seeds
	resX, resY := view.GetSize().X, view.GetSize().Y
	seed := 200 + rand.Float32()*100
	r, g, b := NEBULA_MIN_COLOR+rand.Float32()*NEBULA_MAX_COLOR, NEBULA_MIN_COLOR+rand.Float32()*NEBULA_MAX_COLOR, NEBULA_MIN_COLOR+rand.Float32()*NEBULA_MAX_COLOR

	//send to the shader
	this.SetParams(resX, resY, seed, r, g, b)
}

func (this *Nebula) SetParams(resX, resY float32, seed float32, r, g, b float32) {
	this.shader.SetFloatParameter("resolution", resX, resY)
	this.shader.SetFloatParameter("seed", seed)
	this.shader.SetFloatParameter("colorSeed", r, g, b)
}

func (this *Nebula) Draw(target sf.RenderTarget, delta time.Duration) {
	this.time += delta

	pos := target.GetView().GetCenter()

	oldView := target.GetView()
	target.SetView(target.GetDefaultView())

	this.shape.SetSize(target.GetView().GetSize())
	this.shader.SetFloatParameter("offset", pos.X/10, -pos.Y/10)
	this.shader.SetFloatParameter("time", float32(this.time)/1e11)
	this.shape.Draw(target, sf.RenderStates{sf.BlendNone, sf.TransformIdentity(), this.shader, nil})

	target.SetView(oldView)
}

func (this *Nebula) GetLayer() int {
	return 0
}

func (this *Nebula) IsVisible() bool {
	return true
}
